<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Tarefa extends Model
{
    protected $connection = 'mysql';
    protected $table = 'SGBD_tarefa';
    protected $primaryKey = 'TARE_id';
	public $dates = ['TARE_dt_criacao','TARE_dt_update'];
   
    const CREATED_AT = 'TARE_dt_criacao';
    const UPDATED_AT = 'TARE_dt_update';

    //campos
    public static $tabela = 'SGBD_tarefa';
    public static $id = 'TARE_id';
    public static $fkprojeto = 'TARE_FK_PROJ_id';
    public static $fkusuario = 'TARE_FK_USUA_id';
    public static $titulo = 'TARE_titulo';
    public static $descricao = 'TARE_descricao';
    public static $status = 'TARE_status';
    public static $peso = 'TARE_peso';
    public static $dataInicio = 'TARE_dt_inicio';
    public static $dataFim = 'TARE_dt_fim';
    public static $dataCriacao = 'TARE_dt_criacao';

    //Relacionamento
    public function Usuario(){return $this->belongsTo('App\User',Tarefa::$fkusuario);}
    public function Projeto(){return $this->belongsTo('App\Projeto',Tarefa::$fkprojeto);}
    
    //Get's
    public function getId(){return $this->attributes[Tarefa::$id];}
    public function getFkProjeto(){return $this->attributes[Tarefa::$fkprojeto];}
    public function getFkUsuario(){return $this->attributes[Tarefa::$fkusuario];}
    public function getTitulo(){return $this->attributes[Tarefa::$titulo];}
    public function getDescricao(){return $this->attributes[Tarefa::$descricao];}
    public function getStatus(){return $this->attributes[Tarefa::$status];}
    public function getPeso(){return $this->attributes[Tarefa::$peso];}
    public function getDataInicio(){return $this->attributes[Tarefa::$dataInicio];}
    public function getDataFim(){return $this->attributes[Tarefa::$dataFim];}
    public function getDataCriacao(){return (new Carbon($this->attributes[Tarefa::$dataCriacao]))->format('d/m/Y');}

    //Set's
    public function setFkProjeto($valor){$this->attributes[Tarefa::$fkprojeto] = $valor;}
    public function setFkUsuario($valor){$this->attributes[Tarefa::$fkusuario] = $valor;}
    public function setTitulo($valor){$this->attributes[Tarefa::$titulo] = $valor;}
    public function setDescricao($valor){$this->attributes[Tarefa::$descricao] = $valor;}
    public function setStatus($valor){$this->attributes[Tarefa::$status] = $valor;}
    public function setPeso($valor){$this->attributes[Tarefa::$peso] = $valor;}
    public function setDataInicio($valor){$this->attributes[Tarefa::$dataInicio] = $valor;}
    public function setDataFim($valor){$this->attributes[Tarefa::$dataFim] = $valor;}
}