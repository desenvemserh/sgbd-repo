<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissao extends Model
{
    protected $connection = 'mysql';
    protected $table = 'TBG_permissao';
    protected $primaryKey = 'PERM_id';
    public $timestamps = false;

    //campos
    public static $tabela = 'TBG_permissao';
    public static $id = 'PERM_id';
    public static $fk_classe = 'PERM_FK_CLA_id';
    public static $fk_usuario = 'PERM_FK_USUA_id';

    //relacionamentos
    public function Usuario()
    {
        return $this->belongsTo('App\User',Permissao::$fk_usuario);
    }

    public function Classe()
    {
        return $this->belongsTo('App\Classe',Permissao::$fk_classe);
    }

    //GET'S
    public function getId()
    {
        return $this->attributes[Permissao::$id];
    }
    public function getFkClasse()
    {
        return $this->attributes[Permissao::$fk_classe];
    }
    public function getFkUsuario()
    {
        return $this->attributes[Permissao::$fk_usuario];
    }

    //SET'S
    public function setFkClasse($valor)
    {
        $this->attributes[Permissao::$fk_classe] = $valor;
    }
    public function setFkUsuario($valor)
    {
        $this->attributes[Permissao::$fk_usuario] = $valor;
    }
}
