<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    protected $connection = 'mysql';
    protected $table = 'SGBD_projeto';
    protected $primaryKey = 'PROJ_id';
	public $timestamps = false;

    //campos
    public static $tabela = 'SGBD_projeto';
    public static $id = 'PROJ_id';
    public static $fksistema = 'PROJ_FK_SIST_id';
    public static $progresso = 'PROJ_progresso';
    public static $status = 'PROJ_status';
    public static $datacriacao = 'PROJ_data_criacao';

    //Relacionamento
    public function Sistema(){return $this->belongsTo('App\Sistema',Projeto::$fksistema);}
    public function Tarefa(){return $this->hasMany('App\Tarefa',Tarefa::$fkprojeto);}
    public function Peso(){return $this->hasMany('App\Tarefa',Tarefa::$fkprojeto)->value(Tarefa::$peso);}
    public function Status(){return $this->hasMany('App\Tarefa',Tarefa::$fkprojeto)->value(Tarefa::$status);}
    public function Usuarios()
    {
        return $this->belongsToMany('App\User',ProjetoUsuario::$tabela,ProjetoUsuario::$fkprojeto,ProjetoUsuario::$fkusuario);
    }
    public function ProjetoUsuario(){return $this->hasMany('App\ProjetoUsuario',ProjetoUsuario::$fkprojeto);}
    public function ProjetoId(){return $this->hasMany('App\ProjetoUsuario',ProjetoUsuario::$fkprojeto)->value(ProjetoUsuario::$fkprojeto);}

    //Get's
    public function getId(){return $this->attributes[Projeto::$id];}
    public function getFkSistema(){return $this->attributes[Projeto::$fksistema];}
    public function getProgresso(){return $this->attributes[Projeto::$progresso];}
    public function getStatus(){return $this->attributes[Projeto::$status];}

    //Set's
    public function setFkSistema($valor){$this->attributes[Projeto::$fksistema] = $valor;}
    public function setProgresso($valor){$this->attributes[Projeto::$progresso] = $valor;}
    public function setStatus($valor){$this->attributes[Projeto::$status] = $valor;}
    public function setDataCriacao($valor){$this->attributes[Projeto::$datacriacao] = $valor;}
}