<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Produto;

class ProdutoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     //############ PRODUTO ############
     public function indexProduto(Request $resquest)
     {
         try
         {
             $produtos = Produto::all();
 
             return view('adm.produtos',['produtos'=>$produtos]);
         }
         catch(\Exception $ex)
         {
             return Redirect::back()->withErrors('erro ao carregar os Produtos: '.$ex->getMessage());
         }
     }

     public function salvarProduto(Request $request)
    {
        try
        {
            $prd = Produto::where(Produto::$descricao,$request->descricao)->first();

            if((empty($request->id) && !empty($prd)) || (!empty($request->id) && !empty($prd) && $request->id != $prd->getId()))
            {
                return Redirect::back()->withErrors('Já existe um produto com esta descrição.');
            }

            $produto = Produto::findOrNew($request->id);
            $produto->setNcm($request->ncm);
            $produto->setDescricao($request->descricao);

            $produto->save();
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao salvar o produto: '.$ex->getMessage());
        }

        return Redirect::route('adm.produto');
    }

    public function deleteProduto(Request $request)
    {
        try
        {
            if(empty($request->id))
            {
                return Redirect::back()->withErrors('ID do produto vazio.');
            }

            $produto = Produto::find($request->id);
            $produto->delete();
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao excluir o produto: '.$ex->getMessage());
        }

        return Redirect::route('adm.produto');
    }
}
