<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Subtarefa;
use App\Log;

class SubTarefaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listarSubtarefas(Request $request)
    {
        $subtarefa = Subtarefa::where(Subtarefa::$fktarefa,$request->tarefa)
                                ->get()
                                ->map(function($item){
                                    return
                                        [
                                            'id'=>$item->getId(),
                                            'titulo'=>$item->getTitulo(),
                                            'descricao'=>$item->getDescricao(),
                                            'status'=>$item->getStatus()
                                        ];
                                });
        return $subtarefa;
    }

    public function salvarSubtarefa(Request $request)
    {
        // if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('projeto.projeto');}

        try
        {
            // dd($request);
            $subtarefa = Subtarefa::findOrNew($request->id);
            $subtarefa->setFkTarefa($request->tarefa);
            $subtarefa->setTitulo($request->titulo);
            $subtarefa->setDescricao($request->descricao);
            if($request->id)
            {
                $subtarefa->setStatus('1');
            }
            else
            {
                $subtarefa->setStatus('0');
            }

            $subtarefa->save();
        }
        catch(\Exception $ex)
        {
            return Redirect::route('projeto.tarefa')->withErrors('erro ao salvar a subtarefa: '.$ex->getMessage());
        }
    }
    
    /**
     * Deletar a Subtarefa
     */
        public function deleteSubtarefa(Request $request)
        {
            try
            {
                if(empty($request->subtarefa))
                {
                    return Redirect::back()->withErrors('Id da Subtarefa vazia.');
                }
    
                $subtarefa = Subtarefa::find($request->subtarefa);
                $subtarefa->delete();
            }
            catch(\Exception $ex)
            {
                return Redirect::back()->withErrors('Erro ao excluir a subtarefa: '.$ex->getMessage());
            }
        }

    /**
     * Finalizar Subtarefa
     */
        public function finalizarSubtarefa(Request $request)
        {
            try
            {
                $subtarefa = Subtarefa::find($request->subtarefa);
                $subtarefa->setStatus('1');
                $subtarefa->save();
            }
            catch(\Exception $ex)
            {
                return Redirect::back()->withErrors('Erro ao excluir a subtarefa: '.$ex->getMessage());
            }
        }
}