<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Classe_Rota;
use App\Chamado;
use App\Http\Enum\STATUS;
use App\User;
use App\Sistema;
use App\Projeto;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //recupera a rota padrão
        $rotaPadrao = Classe_Rota::where(Classe_rota::$fk_classe,Auth::user()->Classe->first()->getId())
                            ->where(Classe_rota::$rota_padrao,true)->first();

        return ($rotaPadrao != null && $rotaPadrao->Rota->getRota() != 'home' ? Redirect::route($rotaPadrao->Rota->getRota()) : $this->ViewHome());
    }

    public function ViewHome()
    {
        //Total Usuários
        $usuariosT = DB::table('TBG_usuario')->get();
        $usuarios = $usuariosT->count();

        //Total Funcionários
        $funcionariosT = DB::table('TBG_funcionario')->get();
        $funcionarios = $funcionariosT->count();

        //Total Sistemas
        $sistemasT = DB::table('TBG_sistema')->get();
        $sistemas = $sistemasT->count();

        //Total Sistemas
        $permissoesT = DB::table('TBG_permissao')->get();
        $permissoes = $permissoesT->count();

        //Projeto
        $projetos = Projeto::all();

        return View('home',
        [
            'usuarios'=>$usuarios,
            'funcionarios'=>$funcionarios,
            'sistemas'=>$sistemas,
            'permissoes'=>$permissoes,
            'projetos'=>$projetos
        ]);
    }
}
