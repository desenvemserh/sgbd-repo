<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ProjetoUsuario;
use App\Sistema;
use App\Projeto;

class ComboController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getComboDesenvolvedor()
    {
        return User::whereIn(User::$id,[5135,5136,59,230])->orderby(User::$nome)->get()
        ->keyBy(User::$id)->map(function ($item) 
        {
            return $item->getNome();
        });
    }

    public static function getComboDesenvolvedorParametro($projeto)
    {
        return User::join(ProjetoUsuario::$tabela,User::$id,'=',ProjetoUsuario::$fkusuario)
        ->join(Projeto::$tabela,ProjetoUsuario::$fkprojeto,'=',Projeto::$id)
        ->where(Projeto::$id,$projeto)->orderby(User::$nome)->get()
        ->keyBy(User::$id)->map(function ($item) 
        {
            return $item->getNome();
        });
    }
}