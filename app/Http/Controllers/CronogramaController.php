<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Projeto;
use App\Tarefa;
use App\Sistema;
use Carbon\Carbon;

class CronogramaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexCronograma($prefixo,$projeto,$sufixo)
    {
        $projeto = Projeto::find($projeto);
        return view('projeto.cronograma',['projeto'=>$projeto->getFkSistema()]);
    }

    public function ajaxResultado(Request $request)
    {
        try
        {
            $tarefa = Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                                ->join(Sistema::$tabela,Projeto::$fksistema,'=',Sistema::$id)
                                    ->where(Projeto::$fksistema,$request->sistema)->value(Sistema::$descricao);

            $aFazer=[];
            $execucao=[];
            $feito=[];
            $nome = $tarefa;
                
            //A Fazer
            $anoI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('Y');
            $mesI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','0')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('m');
            $diaI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','0')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('d');
            $anoF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','0')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2021)
                            ->max(Tarefa::$dataFim))->format('Y');
            $mesF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','0')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2021)
                            ->max(Tarefa::$dataFim))->format('m');
            $diaF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','0')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2021)
                            ->max(Tarefa::$dataFim))->format('d');
            array_push($aFazer, $anoI,$mesI - 1,$diaI,$anoF,$mesF - 1,$diaF);

            //Execução
            $anoI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','1')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('Y');
            $mesI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','1')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('m');
            $diaI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','1')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('d');
            $anoF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','1')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->max(Tarefa::$dataFim))->format('Y');
            $mesF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','1')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->max(Tarefa::$dataFim))->format('m');
            $diaF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','1')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->max(Tarefa::$dataFim))->format('d');
            array_push($execucao, $anoI,$mesI - 1,$diaI,$anoF,$mesF - 1,$diaF);

            //Feito
            $anoI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','2')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('Y');
            $mesI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','2')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('m');
            $diaI = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','2')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2020)
                            ->min(Tarefa::$dataInicio))->format('d');
            $anoF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','2')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2021)
                            ->max(Tarefa::$dataFim))->format('Y');
            $mesF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','2')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2021)
                            ->max(Tarefa::$dataFim))->format('m');
            $diaF = Carbon::parse(Tarefa::join(Projeto::$tabela,Tarefa::$fkprojeto,'=',Projeto::$id)
                            ->where(Projeto::$fksistema,$request->sistema)
                            ->where(Tarefa::$status,'=','2')
                            // ->whereYear(Tarefa::$dataInicio, '=' ,2021)
                            ->max(Tarefa::$dataFim))->format('d');
            array_push($feito, $anoI,$mesI - 1,$diaI,$anoF,$mesF - 1,$diaF);

            return response()->json([$aFazer, $execucao, $feito, $nome],200);
        }
        catch(\Exception $ex)
        {
            return response()->json('Erro ao buscar os dados de resultados por mês: '.$ex->getMessage(),500);
        }
    }
}