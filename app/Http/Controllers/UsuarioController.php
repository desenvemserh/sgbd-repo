<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\User;
use Hash;
use App\ViewUsuario;

class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexUsuario(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}
        try
        {
            // $usuarios = ViewUsuario::all();
            $usuarios = User::orderBy(User::$nome)->paginate(10);
            $classes = ClasseController::GetComboClasse();

            return View('adm.usuarios',['usuarios'=>$usuarios,'classes'=>$classes]);
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao carregar os usuários: '.$ex->getMessage());
        }
    }

    public function salvarUsuario(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.usuario');}
        try
        {
            $cpf = str_replace('-','',str_replace('.','',$request->cpf));

            $user = User::where(User::$cpf,$cpf)->first();

            if((empty($request->id) && !empty($user)) || (!empty($request->id) && !empty($user) && $user->getId() != $request->id))
            {
                return Redirect::back()->withErrors('Já existe um usuário cadastrado com o CPF informado.');
            }

            $usuario = User::findOrNew($request->id);
            $usuario->setNome($request->nome);
            $usuario->setEmail($request->email);
            $usuario->setCpf($cpf);
            // $usuario->setTelefone($request->telefone);
            // $usuario->setCelular($request->celular);
            $usuario->setAtivo(!empty(($request->ativo)));
            //$usuario->setAtivo(!empty(($request->ativo) ? '1' : '0'));

            if(empty($request->id) || (!empty($request->id) && !empty($request->senha)))
            {
                $usuario->setSenha(Hash::make($request->senha));
            }

            // if(!empty($request->foto)){
            //     $usuario->setFoto($request->foto);
            // }

            // if($request->hasFile('foto'))
            // {
            //     $usuario->setFoto('storage/'.Util::SalvarArquivo($request->file('foto'),'desenvolvedores',null));         
            // }

            $usuario->save();

            return Redirect::route('adm.usuario');
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao salvar o usuário: '.$ex->getMessage());
        }
    }

    public function deleteUsuario(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.usuario');}
        try
        {
            $usuario = User::find($request->id);
            $usuario->delete();

            return Redirect::route('adm.usuario');
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao excluir o usuário: '.$ex->getMessage());
        }
    }

    public static function getComboUsuario()
    {
        return User::where('USUA_nome', '!=', ' ')->orderby(User::$nome)->get()
        ->keyBy(User::$id)->map(function ($item)
        {
            return $item->getNome();
        });
    }

    public function pesquisaUsuario(Request $request) 
    {
        try
        {
            $cpf = Util::LimpaCpf($request->cpf);
            if(!empty($request->nome)){
                $usuarios = User::where(User::$nome, 'like', '%'.$request->nome.'%')->paginate();
            }
            else if(!empty($request->cpf)){
                $usuarios = User::where(User::$cpf, 'like', '%'.$cpf.'%')->paginate();
            }
            else{
                $usuarios = User::orderBy(User::$nome)->paginate(10);
            }
            $classes = ClasseController::GetComboClasse();
            return View('adm.usuarios',['usuarios'=>$usuarios,'classes'=>$classes]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route('adm.usuarios')->withErrors('erro ao carregar os usuários: '.$ex->getMessage());
        }
    }
}
