<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teste;

class TesteController extends Controller
{
   public function indexTeste()
   {
        $tabela = Teste::all();

        return view('teste',['linhas'=>$tabela]);
   }
}
