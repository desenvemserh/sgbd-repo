<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Estado;

class UfController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexUf(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}

        try
        {
            $ufs = Estado::orderby(Estado::$nome)->get();

            return view('adm.uf',['ufs'=>$ufs]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route('home')->withErrors('erro ao carregar os estados: '.$ex->getMessage());
        }
    }

    public function salvarEstado(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.uf');}

        try
        {
            if(!empty(Estado::where(Estado::$nome,$request->nome)->first()))
            {
                return Redirect::route('adm.uf')->withErrors('Já existe um Estado com este nome.');
            }

            $uf = Estado::findOrNew($request->id);
            $uf->setNome($request->nome);
            $uf->setSigla($request->sigla);
            $uf->save();

            return Redirect::route('adm.uf');
        }
        catch(\Exception $ex)
        {
            return Redirect::route('adm.uf')->withErrors('erro ao salvar o Estado: '.$ex->getMessage());
        }
    }

    public static function GetComboEstado()
    {
        return Estado::orderby(Estado::$nome)->get()
        ->keyBy(Estado::$id)->map(function ($item) 
        {
            return $item->getNome();
        });
    }
}
