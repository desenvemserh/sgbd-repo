<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Funcionario;

class FuncionarioController extends Controller
{
    public function ajaxListaFuncionario(Request $request)
    {
        try
        {
            //Seleciona dados da operação
            $sql = "SELECT f.FUNC_id as id, f.FUNC_nome as nome 
                        from TBG_funcionario f
                        ORDER BY f.FUNC_nome ASC ";

            //executa a query e guarda o resultado
            $resultado =\DB::select($sql);

            
            $dados = ['consulta'=>$resultado];
            
            return response()->json($dados,200);
        }
        catch(\Exception $ex)
        {
            return response()->json('erro ao buscar os dados da operação: '.$ex->getMessage(),500);
        }

    }

    public function indexFuncionario()
    {
        $funcionarios = Funcionario::orderBy(Funcionario::$nome)->paginate(10);
        return view('adm.funcionario',['funcionarios'=>$funcionarios]);
    }

    public function pesquisaFuncionario(Request $request) {

        try
        {
            $cpf = Util::LimpaCpf($request->cpf);
            if(!empty($request->nome)){
                $funcionarios = Funcionario::where(Funcionario::$nome, 'like', '%'.$request->nome.'%')->paginate();
            }
            else if(!empty($request->cpf)){
                $funcionarios = Funcionario::where(Funcionario::$cpf, 'like', '%'.$cpf.'%')->paginate();
            }
            else if(!empty($request->identidade)){
                $funcionarios = Funcionario::where(Funcionario::$identidade, 'like', '%'.$request->identidade.'%')->paginate();
            }
            else{
                $funcionarios = Funcionario::orderBy(Funcionario::$nome)->paginate(10);
            }
            return View('adm.funcionario',['funcionarios'=>$funcionarios]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route('adm.funcionario')->withErrors('erro ao carregar os funcionários: '.$ex->getMessage());
        }
    }
}