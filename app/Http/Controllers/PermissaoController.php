<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Permissao;
use App\Classe;

class PermissaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $rota_principal = 'adm.permissao';

     //############ PERMISSÃO ############
    public function indexPermissao(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}
        try
        {
            $permissoes = Permissao::all();
            $usuarios = UsuarioController::getComboUsuario();
            $sistemas = SistemaController::getComboSistema();

            return view('adm.permissao',['permissoes'=>$permissoes,'usuarios'=>$usuarios,'sistemas'=>$sistemas]);
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao carregar as permissoes: '.$ex->getMessage());
        }
    }

    public function salvarPermissao(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.permissao');}
        try
        {
            $p = Permissao::whereHas('Classe',function($classe) use($request)
                        {
                            $classe->where(Classe::$fk_sistema,$request->sistema);
                        })
                        ->where(Permissao::$fk_usuario,$request->usuario)->first();

            if((empty($request->id) && !empty($p)) || (!empty($request->id) && !empty($p) && $request->id != $p->getId()))
            {
                return Redirect::route($this->rota_principal)->withErrors('Já existe uma permissão do usuário, para este sistema.');
            }
            // $p = Classe::where(Classe::$fk_sistema,$request->sistema)->first();

            // if((empty($request->id) && !empty($p)) || (!empty($request->id) && !empty($p) && $request->id != $p->getId()))
            // {
            //     return Redirect::back()->withErrors('Já existe uma permissão, para o usuário nesse sistema.');
            // }

            $permissao = Permissao::findOrNew($request->id);
            $permissao->setFkClasse($request->classe);
            $permissao->setFkUsuario($request->usuario);

            $permissao->save();
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao salvar o parâmetro: '.$ex->getMessage());
        }

        return Redirect::route('adm.permissao');
    }

    public function deletePermissao(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.permissao');}
        try
        {
            if(empty($request->id))
            {
                return Redirect::back()->withErrors('Código da permissão vazio.');
            }

            $permissao = Permissao::find($request->id);
            $permissao->delete();

            return Redirect::route('adm.permissao');
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao excluir a permissão: '.$ex->getMessage());
        }

        return Redirect::route('adm.permissao');
    }
}
