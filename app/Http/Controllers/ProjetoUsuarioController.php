<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Projeto;
use App\User;
use App\ProjetoUsuario;

class ProjetoUsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //############### PROJETO X USUÁRIO ###############
    public function ajaxCarregaProjetoByUsuario(Request $request)
    {
        try
        {
            // $ids = Sistema::find($request->id)->Usuarios->pluck(User::$id);

            $ids = Projeto::find($request->id)->Usuarios->pluck(User::$id);

            $dados = $this->UsuarioDualList($ids);

            return response()->json($dados,200);
        }
        catch(\Exception $ex)
        {
            return response()->json('erro ao buscar os usuários do projeto: '. Util::TrataMensagemErro('pdo',$ex),500);
        }
    }

    public function ajaxAssociaProjetoUsuario(Request $request)
    {
        try
        {
            if($request->associa == 'true')
            {
                foreach($request->usuarios as $usuario)
                {
                    $vincula = New ProjetoUsuario;

                    $vincula->setFkProjeto($request->idProjeto);
                    $vincula->setFkUsuario($usuario['id']);
                    $vincula->save();
                }
            }
            else
            {

                foreach($request->usuarios as $usuario)
                {
                    $proju = ProjetoUsuario::where(ProjetoUsuario::$fkprojeto,$request->idProjeto)
                            ->where(ProjetoUsuario::$fkusuario,$usuario['id'])
                            ->first()
                            ->delete();
                }
            }

            return response()->json(200);
        }
        catch(\Exception $ex)
        {
            return response()->json('erro ao associar os usuários ao projeto: '. Util::TrataMensagemErro('pdo',$ex),500);
        }
    }

    //############### BUSCA USUÁRIOS VINCULADOS ###############
    /**
     * retorna os Usuários disponíveis e vinculados ao projeto
     */
    private function UsuarioDualList($ids_usuario)
    {
        //usuários disponíveis
        $disponiveis = User::whereNotIn(User::$id,$ids_usuario)
                    ->whereIn(User::$id,[5135,5136,59,230])
                    ->orderby(User::$nome)
                    ->get()
                    ->map(function($item)
                    {
                        return ['id'=> $item->getId(),'label'=> $item->getNome()];
                    });

        //usuários vinculadas
        $vinculados = User::whereIn(User::$id,$ids_usuario)
                    ->orderby(User::$nome)
                    ->get()
                    ->map(function($item)
                    {
                        return ['id'=> $item->getId(),'label'=> $item->getNome()];
                    });

        return ['available'=>$disponiveis,'selected'=>$vinculados];
    }
}