<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Projeto;
use App\Tarefa;
use App\Log;
use Carbon\Carbon;

class ProjetoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //############ PROJETO ############
        public function indexProjeto(Request $request)
        {
            if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('projeto.projeto');}

            try
            {
                $projetos = Projeto::orderby(Projeto::$id)->get();
                $sistema = SistemaController::getComboSistema();
                $desenvolvedor = ComboController::getComboDesenvolvedor();
                $i = 1;

                return view('projeto.projetos',['projetos'=>$projetos,
                                'sistema'=>$sistema,'i'=>$i,'desenvolvedor'=>$desenvolvedor]);
            }
            catch(\Exception $ex)
            {
                return Redirect::back()->withErrors('erro ao carregar os Projetos: '.$ex->getMessage());
            }
        }

        public function salvarProjeto(Request $request)
        {
            // if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('projeto.projeto');}

            try
            {
                $project = Projeto::where(Projeto::$fksistema,$request->sistema)->first();

                if((empty($request->id) && !empty($project)) || (!empty($request->id) && !empty($project) && $project->getId() != $request->id))
                {
                    return Redirect::route('projeto.projeto')->withErrors('Já existe um projeto cadastrado, para este sistema!');
                }

                $projeto = Projeto::findOrNew($request->id);
                $projeto->setFkSistema($request->sistema);
                $projeto->setProgresso(0);
                $projeto->setStatus('0');
                $projeto->setDataCriacao(Carbon::now());

                $projeto->save();
            }
            catch(\Exception $ex)
            {
                return Redirect::route('projeto.projeto')->withErrors('erro ao salvar o projeto: '.$ex->getMessage());
            }

            return Redirect::route('projeto.projeto');
        }

    /**
     * Lista Pedidos para Aprovação
    */
    public function indexVisualizarTarefas($prefixo,$projeto,$sufixo)
    {
    //verifica acesso à rota
    // if(!RotaController::Acesso($request->route()->getName()))
    // {
    //     return RotaController::AcessoNegado('home');
    // } 
        try
        {
            $projetos = Projeto::find($projeto);
            $sistema = SistemaController::getComboSistema();
            $desenvolvedor = ComboController::getComboDesenvolvedorParametro($projeto);
            $tarefas = Tarefa::where(Tarefa::$fkprojeto,$projeto)->get();
            $i = 1;
            $valor = $tarefas->count();

            if(is_null($tarefas) || $tarefas->count() == 0)
            {
                $total = 0;
                
            }else{
                $totalT = 0;
                $totalF = 0;
                foreach($tarefas as $tot){
                    $totalT +=($tot->getPeso());
                    if($tot->getStatus() == 2){
                        $totalF +=($tot->getPeso());
                    }
                }

                $total = ($totalF * 100) / $totalT;
            }
                
            return view('projeto.visualizartarefas', ['projetos'=>$projetos,'sistema'=>$sistema,
                            'desenvolvedor'=>$desenvolvedor,'i'=>$i,'tarefas'=>$tarefas,
                            'valor'=>$valor,'total'=>$total]);
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('Erro ao carregar o projeto e as tarefas: '.$ex->getMessage());
        
        }
    }

    public function ajaxTarefaAFazer(Request $request)
    {
        $tarefaAberta = Tarefa::where(Tarefa::$fkprojeto,$request->projeto)->where(Tarefa::$status,$request->status)
                                ->get()
                                ->map(function($item){
                                    return
                                        [
                                            'id'=>$item->getId(),
                                            'titulo'=>$item->getTitulo(),
                                            'descricao'=>$item->getDescricao(),
                                            'foto'=>$item->Usuario->getFoto(),
                                            'peso'=>$item->getPeso(),
                                            'dataCriacao'=>$item->getDataCriacao(),
                                            'desenvolvedor'=>$item->getFkUsuario(),
                                            'status'=>$item->getStatus(),
                                            'dataInicio'=>!empty($item->getDataInicio()) ? Carbon::parse($item->getDataInicio())->format('Y-m-d') : '',
                                            'dataFim'=>!empty($item->getDataFim()) ? Carbon::parse($item->getDataFim())->format('Y-m-d') : ''
                                        ];
                                });
        return $tarefaAberta;
    }

    public function ajaxTarefaEmExecucao(Request $request)
    {
        $tarefaEmExecucao = Tarefa::where(Tarefa::$fkprojeto,$request->projeto)->where(Tarefa::$status,$request->status)
                                ->get()
                                ->map(function($item){
                                    return
                                        [
                                            'id'=>$item->getId(),
                                            'titulo'=>$item->getTitulo(),
                                            'descricao'=>$item->getDescricao(),
                                            'foto'=>$item->Usuario->getFoto(),
                                            'peso'=>$item->getPeso(),
                                            'dataCriacao'=>$item->getDataCriacao(),
                                            'desenvolvedor'=>$item->getFkUsuario(),
                                            'status'=>$item->getStatus(),
                                            'dataInicio'=>!empty($item->getDataInicio()) ? Carbon::parse($item->getDataInicio())->format('Y-m-d') : '',
                                            'dataFim'=>!empty($item->getDataFim()) ? Carbon::parse($item->getDataFim())->format('Y-m-d') : ''
                                        ];
                                });
        return $tarefaEmExecucao;
    }

    public function ajaxTarefaFeito(Request $request)
    {
        $tarefaFeito = Tarefa::where(Tarefa::$fkprojeto,$request->projeto)->where(Tarefa::$status,$request->status)
                                ->get()
                                ->map(function($item){
                                    return
                                        [
                                            'id'=>$item->getId(),
                                            'titulo'=>$item->getTitulo(),
                                            'descricao'=>$item->getDescricao(),
                                            'foto'=>$item->Usuario->getFoto(),
                                            'peso'=>$item->getPeso(),
                                            'dataCriacao'=>$item->getDataCriacao(),
                                            'desenvolvedor'=>$item->getFkUsuario(),
                                            'status'=>$item->getStatus(),
                                            'dataInicio'=>!empty($item->getDataInicio()) ? Carbon::parse($item->getDataInicio())->format('Y-m-d') : '',
                                            'dataFim'=>!empty($item->getDataFim()) ? Carbon::parse($item->getDataFim())->format('Y-m-d') : ''
                                        ];
                                });
        return $tarefaFeito;
    }

    public function ajaxTarefas(Request $request)
    {
        $tarefas = Tarefa::where(Tarefa::$id,$request->tarefa)
                                ->get()
                                ->map(function($item){
                                    return
                                        [
                                            'id'=>$item->getId(),
                                            'titulo'=>$item->getTitulo(),
                                            'descricao'=>$item->getDescricao(),
                                            'peso'=>$item->getPeso(),
                                            'desenvolvedor'=>$item->getFkUsuario(),
                                            'status'=>$item->getStatus(),
                                            'dataInicio'=>!empty($item->getDataInicio()) ? Carbon::parse($item->getDataInicio())->format('Y-m-d') : '',
                                            'dataFim'=>!empty($item->getDataFim()) ? Carbon::parse($item->getDataFim())->format('Y-m-d') : ''
                                        ];
                                });
        return $tarefas;
    }
}