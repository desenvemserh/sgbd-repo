<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Tarefa;
use App\Projeto;
use App\Log;

class TarefaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function salvarTarefa(Request $request)
        {
            // if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('projeto.projeto');}

            try
            {
                // dd($request);
                $tarefa = Tarefa::findOrNew($request->id);
                $tarefa->setFkProjeto($request->hdnProj);
                $tarefa->setFkUsuario($request->desenvolvedor);
                $tarefa->setTitulo($request->titulo);
                $tarefa->setDescricao($request->descricao);
                $tarefa->setPeso($request->peso);
                if($request->status){
                    $tarefa->setStatus($request->status);
                }else{
                    $tarefa->setStatus('0');
                }
                $tarefa->setDataInicio($request->dataInicio);
                $tarefa->setDataFim($request->dataFim);

                $tarefa->save();

                $tarefas = Tarefa::where(Tarefa::$fkprojeto,$tarefa->getFkProjeto())->get();

                if(is_null($tarefas) || $tarefas->count() == 0)
                {
                    $total = 0;
                    
                }else{
                    $totalT = 0;
                    $totalF = 0;
                    foreach($tarefas as $tot){
                        $totalT +=($tot->getPeso());
                        if($tot->getStatus() == 2){
                            $totalF +=($tot->getPeso());
                        }
                    }

                    $total = ($totalF * 100) / $totalT;
                }

                $projeto = Projeto::find($tarefa->getFkProjeto());
                $projeto->setProgresso($total);
                if($total == 100){
                    $projeto->setStatus('1');
                }else{
                    $projeto->setStatus('0');
                }

                $projeto->save();
            }
            catch(\Exception $ex)
            {
                return Redirect::route('projeto.projeto')->withErrors('erro ao salvar a tarefa: '.$ex->getMessage());
            }

            return Redirect::back();
        }
    
    /**
     * Deletar a Tarefa
     */
        public function deleteTarefa(Request $request)
        {
            try
            {
                if(empty($request->id))
                {
                    return Redirect::back()->withErrors('Id da tarefa vazia.');
                }
    
                $tarefa = Tarefa::find($request->id);
                $tarefa->delete();
            }
            catch(\Exception $ex)
            {
                return Redirect::back()->withErrors('Erro ao excluir a tarefa: '.$ex->getMessage());
            }
    
            return Redirect::back();
        }

        public function atualizarTarefa(Request $request)
        {
            // if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('projeto.projeto');}
            try
            {
                $tarefa = Tarefa::find($request->id);
                $tarefa->setStatus($request->status);

                $tarefa->save();
            }
            catch(\Exception $ex)
            {
                return Redirect::route('projeto.tarefa')->withErrors('erro ao mudar o status da tarefa: '.$ex->getMessage());
            }
        }
}