<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sistema;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Log;
use Hash;

class Util extends Controller
{
    /**
     * Salva o arquivo no servidor e retorna o caminho do arquivo
     */
    public static function SalvarArquivo($arquivo,$pasta,$nome)
    {
        $nameFile = null;
        if(empty($nome))
        {
            $name = md5(uniqid(rand(), true));
            $extension = $arquivo->extension();
            $nameFile= "{$name}.{$extension}";
        }
        else
        {
            $nameFile = $nome;
        }

        return $arquivo->storeAs($pasta, $nameFile);
    }

    /**
     * Busca no BD o sistema em questão
     */
    public static function getSistema()
    {
        return Sistema::where(Sistema::$codigo,env('APP_SISTEMA'))->first();
    }

     /**
     * Retorna o valor com a criptografia do sistema
     */
    public static function Criptografia($valor)
    {
        return Hash::make($valor);
    }

    /**
     * Retira os '.' e '-' do CPF
     */
    public static function LimpaCpf($cpf)
    {
        return str_replace('/','',str_replace('-','',str_replace('.','',$cpf)));
    }

    /**
     * Gera o registro na tabela de log do sistema
     */
    public static function Log($descricao)
    {
        $log = New Log;
        $log->setDescricao($descricao);
        $log->setData(Carbon::now());
        $log->setFkSistema(Util::getSistema()->getId());
        $log->setFkUsuario(Auth::user()->getId());
        $log->save();
    }

    public static function TrataMensagemErro($tipo,$ex)
    {
        $msg = '';

        if(strtolower($tipo) == 'pdo')
        {
            $msg = '[BD] ';
            switch($ex->getCode())
            {
                case '23000':
                    $msg .= 'O registro está em uso por outras tabelas';
                    break;
                default:
                    $msg .= '['. $ex->getCode() .'] '. $ex->getMessage();
                    break;
            }
        }

        return $msg;
    }

    public static function StringAleatoria()
    {
        return md5(uniqid(rand(), true));
    }

    public static function RemoveCaracter($palavra)
    {
        return str_replace('ç','c',$palavra);
    }
}
