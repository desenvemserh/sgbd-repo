<?php

namespace App\Enum;

class PESO_TAREFA
{
    public static $BAIXO = [1,2,3,4];
    public static $MEDIO = [5,6,7];
    public static $DIFICIL = [8,9,10];

    public static function GetTipo($valor)
    {
        if($valor == PESO_TAREFA::$BAIXO)
        {
            return  'BAIXO';
        }
        else if($valor == PESO_TAREFA::$MEDIO)
        {
            return 'MEDIO';
        }
        else if($valor == PESO_TAREFA::$DIFICIL)
        {
            return 'DIFICIL';
        }
    }
    public static function GetBadge($valor)
    {
        if($valor == PESO_TAREFA::$BAIXO)
        {
            return  'info';
        }
        else if($valor == PESO_TAREFA::$MEDIO)
        {
            return 'warning';
        }
        else if($valor == PESO_TAREFA::$DIFICIL)
        {
            return 'danger';
        }   
    }
}
