<?php

namespace App\Enum;

class STATUS_PROJETO
{
    public static $ABERTO = 0;
    public static $FINALIZADO = 1;

    public static function GetTipo($valor)
    {
        if($valor == STATUS_PROJETO::$ABERTO)
        {
            return  'ABERTO';
        }
        else if($valor == STATUS_PROJETO::$FINALIZADO)
        {
            return 'FINALIZADO';
        }
    }
    public static function GetBadge($valor)
    {
        if($valor == STATUS_PROJETO::$ABERTO)
        {
            return  'info';
        }
        else if($valor == STATUS_PROJETO::$FINALIZADO)
        {
            return 'success';
        }   
    }
}
