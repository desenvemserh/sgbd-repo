<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{
    protected $connection = 'mysql';
    protected $table = 'TBG_unidade';
    protected $primaryKey = 'UNID_ID';
    public $timestamps = false;

    //campos
    public static $id = 'UNID_ID';
    public static $fk_cidade = 'UNID_FK_CIDA_id';
    public static $fk_tipo = 'UNID_FK_TIPOU_id';
    public static $cod = 'UNID_codigo';
    public static $nome = 'UNID_descricao';
    public static $endereco = 'UNID_endereco';
    public static $numero = 'UNID_nro';
    public static $complemento = 'UNID_complemento';
    public static $bairro = 'UNID_bairro';
    public static $cep = 'UNID_cep';
    public static $ativo = 'UNID_ativo';
    public static $obs = 'UNID_observacao';
    public static $cod_alterdata = 'UNID_cod_alterdata';


    //relacionamentos
    public function Cidade()
    {
        return $this->belongsTo('App\Cidade',Unidade::$fk_cidade);
    }
    public function Tipo()
    {
        return $this->belongsTo('App\Tipo_Unidade',Unidade::$fk_tipo);
    }
    public function Usuarios()
    {
        return $this->belongsToMany('App\User',Usuario_Unidade::$tabela,Usuario_Unidade::$fk_unidade,Usuario_Unidade::$fk_usuario);
    }
    public function Terceiros()
    {
        return $this->belongsToMany('App\Terceiro',Terceiro_Unidade::$tabela,Terceiro_Unidade::$fk_unidade,Terceiro_Unidade::$fk_terceiro);
    }
    public function Setor()
    {
        return $this->belongsTo('App\Setor',Setor::$fk_unidade);
    }
    public function Funcionario()
    {
        return $this->hasMany('App\Funcionario',Funcionario::$fk_unidade);
    }

    //atributos
    public function getId()
    {
        return $this->attributes[Unidade::$id];
    }
    public function getFkCidade()
    {
        return $this->attributes[Unidade::$fk_cidade];
    }
    public function getFkTipo()
    {
        return $this->attributes[Unidade::$fk_tipo];
    }
    public function getCod()
    {
        return $this->attributes[Unidade::$cod];
    }
    public function getNome()
    {
        return $this->attributes[Unidade::$nome];
    }
    public function getEndereco()
    {
        return $this->attributes[Unidade::$endereco];
    }
    public function getNumero()
    {
        return $this->attributes[Unidade::$numero];
    }
    public function getComplemento()
    {
        return $this->attributes[Unidade::$complemento];
    }
    public function getBairro()
    {
        return $this->attributes[Unidade::$bairro];
    }
    public function getCep()
    {
        return $this->attributes[Unidade::$cep];
    }
    public function getAtivo()
    {
        return $this->attributes[Unidade::$ativo];
    }
    public function getObs()
    {
        return $this->attributes[Unidade::$obs];
    }
    public function getCodAlterdata()
    {
        return $this->attributes[Unidade::$cod_alterdata];
    }


    public function setFkCidade($valor)
    {
        $this->attributes[Unidade::$fk_cidade] = $valor;
    }
    public function setFkTipo($valor)
    {
        $this->attributes[Unidade::$fk_tipo] = $valor;
    }
    public function setCod($valor)
    {
        $this->attributes[Unidade::$cod] = $valor;
    }
    public function setNome($valor)
    {
        $this->attributes[Unidade::$nome] = $valor;
    }
    public function setEndereco($valor)
    {
        $this->attributes[Unidade::$endereco] = $valor;
    }
    public function setNumero($valor)
    {
        $this->attributes[Unidade::$numero] = $valor;
    }
    public function setComplemento($valor)
    {
        $this->attributes[Unidade::$complemento] = $valor;
    }
    public function setBairro($valor)
    {
        $this->attributes[Unidade::$bairro] = $valor;
    }
    public function setCep($valor)
    {
        $this->attributes[Unidade::$cep] = $valor;
    }
    public function setAtivo($valor)
    {
        $this->attributes[Unidade::$ativo] = $valor;
    }
    public function setObs($valor)
    {
        $this->attributes[Unidade::$obs] = $valor;
    }
}
