<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mysql';
    protected $table = 'TBG_usuario';
    protected $primaryKey = 'USUA_id';
    protected $hidden = ['remember_token'];
	public $dates = ['USUA_dt_criacao','USUA_dt_alteracao'];
   
    const CREATED_AT = 'USUA_dt_criacao';
    const UPDATED_AT = 'USUA_dt_alteracao';

    //campos
    public static $tabela = 'TBG_usuario';
    public static $id = 'USUA_id';
    public static $nome = 'USUA_nome';
    public static $cpf = 'USUA_cpf';
    public static $email = 'USUA_email';
    public static $tipo = 'USUA_tipo';
    public static $ativo = 'USUA_ativo';
    public static $foto = 'USUA_foto';
    public static $telefone = 'USUA_telefone';
    public static $celular = 'USUA_celular';
    public static $senha = 'password';
    public static $fk_funcionario = 'USUA_FK_FUNC_id';

    //relacionamentos
    public function Classe()
    {
        return $this->belongsToMany('App\Classe',Permissao::$tabela,Permissao::$fk_usuario,Permissao::$fk_classe)
                    ->whereHas('Sistema',function($sis)
                    {
                        $sis->where(Sistema::$codigo,env('APP_SISTEMA'));
                    });
    }
    public function Funcionario()
    {
        return $this->belongsTo('App\Funcionario',User::$fk_funcionario);
    }
    public function ProjetoUsuario()
    {
        return $this->hasMany('App\ProjetoUsuario',ProjetoUsuario::$fkusuario);
    }

    //atributos
    public function getId()
    {
        return $this->attributes[User::$id];
    }

    public function getNome()
    {
        return $this->attributes[User::$nome];
    }

    public function getCpf()
    {
        return $this->attributes[User::$cpf];
    }

    public function getEmail()
    {
        return $this->attributes[User::$email];
    }

    public function getTipo()
    {
        return $this->attributes[User::$tipo];
    }

    public function getAtivo()
    {
        return $this->attributes[User::$ativo];
    }

    public function getFoto()
    {
        return $this->attributes[User::$foto];
    }

    // public function getTelefone()
    // {
    //     return $this->attributes[User::$telefone];
    // }

    // public function getCelular()
    // {
    //     return $this->attributes[User::$celular];
    // }

    public function getFkFuncionario()
    {
        return $this->attributes[User::$fk_funcionario];
    }

    //Set's
    public function setNome($valor)
    {
        $this->attributes[User::$nome] = $valor;
    }

    public function setCpf($valor)
    {
        $this->attributes[User::$cpf] = $valor;
    }

    public function setEmail($valor)
    {
        $this->attributes[User::$email] = $valor;
    }

    public function setTipo($valor)
    {
        $this->attributes[User::$tipo] = $valor;
    }

    public function setAtivo($valor)
    {
        $this->attributes[User::$ativo] = $valor;
    }

    public function setFoto($valor)
    {
        $this->attributes[User::$foto] = $valor;
    }

    public function setSenha($valor)
    {
        $this->attributes[User::$senha] = $valor;
    }

    // public function setTelefone($valor)
    // {
    //     $this->attributes[User::$telefone] = $valor;
    // }

    // public function setCelular($valor)
    // {
    //     $this->attributes[User::$celular] = $valor;
    // }
}
