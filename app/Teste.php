<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teste extends Model
{
    protected $connection = 'mysql';
    protected $table = 'teste';
    protected $primaryKey = 'teste_id';
    public $timestamps = false;

    //campos da tabela
    public static $id = 'teste_id';
    public static $nome = 'teste_nome';
    public static $idade = 'teste_idade';

}
