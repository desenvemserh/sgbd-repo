<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Funcionario extends Model
{
	protected $connection = 'mysql';
	protected $table = 'TBG_funcionario';
    protected $primaryKey = 'FUNC_id';
	public $timestamps = false;

    //campos
    public static $tabela = 'TBG_funcionario';
	public static $id = 'FUNC_id';
	public static $nome = 'FUNC_nome';
    public static $fk_unidade = 'FUNC_FK_UNIDADE_COD';
    public static $cpf = 'FUNC_cpf';
    public static $identidade = 'FUNC_identidade';
    public static $dt_nascimento = 'FUNC_dt_nascimento';
    public static $dt_admissao = 'FUNC_dt_admissao';
    public static $dt_demissao = 'FUNC_dt_demissao';
    public static $dt_criacao = 'FUNC_dt_criacao';
    public static $dt_alteracao = 'FUNC_dt_alteracao';
    public static $email = 'FUNC_email';

    //Relacionamentos
    public function Usuario()
    {
        return $this->hasMany('App\User',User::$fk_funcionario);
    }
    public function Unidade()
    {
        return $this->belongsTo('App\Unidade',Funcionario::$fk_unidade, Unidade::$cod_alterdata);
    }

	//GET
    public function getId(){return $this->attributes[Funcionario::$id];}
    public function getNome(){return $this->attributes[Funcionario::$nome];}
    public function getFkUnidade(){return $this->attributes[Funcionario::$fk_unidade];}
    public function getCpf(){return $this->attributes[Funcionario::$cpf];}
    public function getEmail(){return $this->attributes[Funcionario::$email];}
    public function getIdentidade(){return $this->attributes[Funcionario::$identidade];}
    public function getDtNascimento(){return (new Carbon($this->attributes[Funcionario::$dt_nascimento]))->format('d/m/Y');}
    public function getDtAdmissao(){return (new Carbon($this->attributes[Funcionario::$dt_admissao]))->format('d/m/Y');}
    public function getDtDemissao(){return (new Carbon($this->attributes[Funcionario::$dt_demissao]))->format('d/m/Y');}
    public function getDtCriacao(){return $this->attributes[Funcionario::$dt_criacao];}
    public function getDtAlteracao(){return $this->attributes[Funcionario::$dt_alteracao];}
}