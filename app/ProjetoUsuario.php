<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjetoUsuario extends Model
{
    protected $connection = 'mysql';
    protected $table = 'SGBD_proj_usuario';
    protected $primaryKey = 'PROJU_id';
    public $timestamps = false;

    //campos
    public static $tabela = 'SGBD_proj_usuario';
    public static $id = 'PROJU_id';
    public static $fkprojeto = 'PROJU_FK_PROJ_id';
    public static $fkusuario = 'PROJU_FK_USUA_id';

    //relacionamentos
    public function Usuario()
    {
        return $this->belongsTo('App\User',ProjetoUsuario::$fkusuario);
    }

    public function Projeto()
    {
        return $this->belongsTo('App\Projeto',ProjetoUsuario::$fkprojeto);
    }
    
    //Get's
    public function getId(){return $this->attributes[ProjetoUsuario::$id];}
    public function getFkProjeto(){return $this->attributes[ProjetoUsuario::$fkprojeto];}
    public function getFkUsuario(){return $this->attributes[ProjetoUsuario::$fkusuario];}

    //Set's
    public function setFkProjeto($valor){$this->attributes[ProjetoUsuario::$fkprojeto] = $valor;}
    public function setFkUsuario($valor){$this->attributes[ProjetoUsuario::$fkusuario] = $valor;}
}