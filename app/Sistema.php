<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistema extends Model
{
    protected $connection = 'mysql';
    protected $table = 'TBG_sistema';
    protected $primaryKey = 'SIST_id';
    public $timestamps = false;

    //campos
    public static $tabela = 'TBG_sistema';
    public static $id = 'SIST_id';
    public static $codigo = 'SIST_codigo';
    public static $descricao = 'SIST_descricao';

    //Relacionamentos
    // public function Foto(){return $this->hasMany('App\SistemaUsuario',SistemaUsuario::$fksistema)->join(User::$tabela,SistemaUsuario::$fkusuario,'=',User::$id)->value(User::$foto);}
    // public function SistemaId(){return $this->hasMany('App\SistemaUsuario',SistemaUsuario::$fksistema)->value(SistemaUsuario::$fksistema);}

    //atributos
    public function getId()
    {
        return $this->attributes[Sistema::$id];
    }
    public function getCodigo()
    {
        return $this->attributes[Sistema::$codigo];
    }
    public function getDescricao()
    {
        return $this->attributes[Sistema::$descricao];
    }


    public function setCodigo($valor)
    {
        $this->attributes[Sistema::$codigo] = $valor;
    }
    public function setDescricao($valor)
    {
        $this->attributes[Sistema::$descricao] = $valor;
    }
}
