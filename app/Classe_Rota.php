<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe_Rota extends Model
{
    protected $connection = 'mysql';
    protected $table = 'TBG_classe_rota';
    protected $primaryKey = 'CLARO_id';
    public $timestamps = false;

    //campos
    public static $tabela = 'TBG_classe_rota';
    public static $id = 'CLARO_id';
    public static $fk_classe = 'CLARO_FK_CLA_id';
    public static $fk_rota = 'CLARO_FK_ROTA_id';
    public static $rota_padrao = 'CLARO_padrao';

    //relacionamentos
    public function Rota()
    {
        return $this->belongsTo('App\Rota',Classe_Rota::$fk_rota);
    }

    public function Classe()
    {
        return $this->belongsTo('App\Classe',Classe_Rota::$fk_classe);
    }


    //campos
    public function isPadrao()
    {
        return $this->attributes[Classe_Rota::$rota_padrao] == 1;
    }

    public function setFkRota($valor)
    {
        $this->attributes[Classe_Rota::$fk_rota] = $valor;
    }
    public function setFkClasse($valor)
    {
        $this->attributes[Classe_Rota::$fk_classe] = $valor;
    }
    public function setPadrao($valor)
    {
        $this->attributes[Classe_Rota::$rota_padrao] = $valor;
    }
}
