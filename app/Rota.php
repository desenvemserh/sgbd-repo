<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rota extends Model
{
    protected $connection = 'mysql';
    protected $table = 'TBG_rotas';
    protected $primaryKey = 'ROTA_id';
    public $timestamps = false;


    //campos
    public static $id = 'ROTA_id';
    public static $fk_sistema = 'ROTA_FK_SIST_id';
    public static $fk_rota_pai = 'ROTA_FK_ROTA_id';
    public static $nome = 'ROTA_nome';
    public static $rota = 'ROTA_rota';
    public static $menu = 'ROTA_menu';
    public static $icone = 'ROTA_icone';
    public static $index = 'ROTA_index';

    //relacionamentos
    public function Sistema()
    {
        return $this->belongsTo('App\Sistema',Rota::$fk_sistema);
    }
    public function Classes()
    {
        return $this->belongsToMany('App\Classe',Classe_Rota::$tabela,Classe_Rota::$fk_rota,Classe_Rota::$fk_classe);
    }
    public function RotaPai()
    {
        return $this->belongsTo('App\Rota',Rota::$fk_rota_pai);
    }
    public function Rotas()
    {
        return $this->hasMany('App\Rota',Rota::$fk_rota_pai)->orderby(Rota::$index);
    }


    //atributos
    public function getId()
    {
        return $this->attributes[Rota::$id];
    }
    public function getNome()
    {
        return $this->attributes[Rota::$nome];
    }
    public function isMenu()
    {
        return $this->attributes[Rota::$menu] == 1;
    }
    public function getIcone()
    {
        return $this->attributes[Rota::$icone];
    }
    public function getRota()
    {
        return $this->attributes[Rota::$rota];
    }
    public function getFkRotaPai()
    {
        return $this->attributes[Rota::$fk_rota_pai];
    }
    public function getFkSistema()
    {
        return $this->attributes[Rota::$fk_sistema];
    }
    public function getIndex()
    {
        return $this->attributes[Rota::$index];
    }


    public function setNome($valor)
    {
        $this->attributes[Rota::$nome] = $valor;
    }
    public function setMenu($valor)
    {
        $this->attributes[Rota::$menu] = $valor;
    }
    public function setIcone($valor)
    {
        $this->attributes[Rota::$icone] = $valor;
    }
    public function setRota($valor)
    {
        $this->attributes[Rota::$rota] = $valor;
    }
    public function setFkRotaPai($valor)
    {
        $this->attributes[Rota::$fk_rota_pai] = $valor;
    }
    public function setFkSistema($valor)
    {
        $this->attributes[Rota::$fk_sistema] = $valor;
    }
    public function setIndex($valor)
    {
        $this->attributes[Rota::$index] = $valor;
    }
}
