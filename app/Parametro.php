<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{
    protected $connection = 'mysql';
    protected $table = 'PATIO_parametro';
    protected $primaryKey = 'PARA_id';
    public $timestamps = false;

    //campos
    public static $id = 'PARA_id';
    public static $codigo = 'PARA_codigo';
    public static $descricao = 'PARA_descricao';
    public static $valor = 'PARA_valor';

    //atributos
    public function getId()
    {
        return $this->attributes[Parametro::$id];
    }
    public function getCodigo()
    {
        return $this->attributes[Parametro::$codigo];
    }
    public function getDescricao()
    {
        return $this->attributes[Parametro::$descricao];
    }
    public function getValor()
    {
        return $this->attributes[Parametro::$valor];
    }


    public function setCodigo($valor)
    {
        $this->attributes[Parametro::$codigo] = $valor;
    }
    public function setDescricao($valor)
    {
        $this->attributes[Parametro::$descricao] = $valor;
    }
    public function setValor($valor)
    {
        $this->attributes[Parametro::$valor] = $valor;
    }
}
