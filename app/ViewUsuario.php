<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewUsuario extends Model
{
    protected $connection = 'mysql';
    protected $table = 'v_usuario';
    protected $primaryKey = 'id';
    public $timestamps = false;

    //Campos
    public static $id = 'id';
    public static $cpf = 'cpf';
    public static $usua_nome = 'usua_nome';
    public static $email = 'email';
    public static $ativo = 'ativo';
    public static $identidde = 'identidade';
    public static $nome = 'nome';
    public static $dt_nascimento = 'dt_nascimento';
    public static $dt_admissao = 'dt_admissao';
    public static $dt_demissao = 'dt_demissao';
    public static $unidade = 'descricao';

    //Get's
    public function getId()
    {
        return $this->attributes[ViewUsuario::$id];
    }
    public function getCpf()
    {
        return $this->attributes[ViewUsuario::$cpf];
    }
    public function getUsuaNome()
    {
        return $this->attributes[ViewUsuario::$usua_nome];
    }
    public function getEmail()
    {
        return $this->attributes[ViewUsuario::$email];
    }
    public function getAtivo()
    {
        return $this->attributes[ViewUsuario::$ativo];
    }
    public function getIdentidade()
    {
        return $this->attributes[ViewUsuario::$identidade];
    }
    public function getNome()
    {
        return $this->attributes[ViewUsuario::$nome];
    }
    public function getDataNascimento()
    {
        return $this->attributes[ViewUsuario::$dt_nascimento];
    }
    public function getAdmissao()
    {
        return $this->attributes[ViewUsuario::$dt_admissao];
    }
    public function getDemissao()
    {
        return $this->attributes[ViewUsuario::$demissao];
    }
    public function getUnidade()
    {
        return $this->attributes[ViewUsuario::$unidade];
    }
}