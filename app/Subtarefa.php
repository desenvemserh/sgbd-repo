<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Subtarefa extends Model
{
    protected $connection = 'mysql';
    protected $table = 'SGBD_subtarefa';
    protected $primaryKey = 'SUBTARE_id';
	public $dates = ['SUBTARE_dt_criacao','SUBTARE_dt_update'];
   
    const CREATED_AT = 'SUBTARE_dt_criacao';
    const UPDATED_AT = 'SUBTARE_dt_update';

    //campos
    public static $tabela = 'SGBD_subtarefa';
    public static $id = 'SUBTARE_id';
    public static $fktarefa = 'SUBTARE_FK_TARE_id';
    public static $titulo = 'SUBTARE_titulo';
    public static $descricao = 'SUBTARE_descricao';
    public static $status = 'SUBTARE_status';
    public static $dataCriacao = 'SUBTARE_dt_criacao';

    //Relacionamento
    public function Tarefa(){return $this->belongsTo('App\Tarefa',Subtarefa::$fktarefa);}
    
    //Get's
    public function getId(){return $this->attributes[Subtarefa::$id];}
    public function getFkTarefa(){return $this->attributes[Subtarefa::$fktarefa];}
    public function getTitulo(){return $this->attributes[Subtarefa::$titulo];}
    public function getDescricao(){return $this->attributes[Subtarefa::$descricao];}
    public function getStatus(){return $this->attributes[Subtarefa::$status];}
    public function getDataCriacao(){return (new Carbon($this->attributes[Subtarefa::$dataCriacao]))->format('d/m/Y');;}

    //Set's
    public function setFkTarefa($valor){$this->attributes[Subtarefa::$fktarefa] = $valor;}
    public function setTitulo($valor){$this->attributes[Subtarefa::$titulo] = $valor;}
    public function setDescricao($valor){$this->attributes[Subtarefa::$descricao] = $valor;}
    public function setStatus($valor){$this->attributes[Subtarefa::$status] = $valor;}
}