<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $connection = 'mysql';
    protected $table = 'TBG_uf';
    protected $primaryKey = 'UF_id';
    public $timestamps = false;


    //campos
    public static $id = 'UF_id';
    public static $nome = 'UF_descricao';
    public static $sigla = 'UF_sigla';

    //relacionamentos
    public function Cidades()
    {
        return $this->hasMany('App\Cidade',Cidade::$fk_estado);
    }

    //atributos
    public function getId()
    {
        return $this->attributes[Estado::$id];
    }

    public function getNome()
    {
        return $this->attributes[Estado::$nome];
    }

    public function getSigla()
    {
        return $this->attributes[Estado::$sigla];
    }

    public function setNome($valor)
    {
        $this->attributes[Estado::$nome] = $valor;
    }

    public function setSigla($valor)
    {
        $this->attributes[Estado::$sigla] = $valor;
    }
}
