function MostraListaRotas(id_classe,dados)
{
   lista = $('#pick').pickList(
    {
        data: dados,
        buttons:
        [
            {
                action: 'add',
                label: '',
                className: 'btn btn-sm btn-block btn-success fas fas fa-angle-right'
            },
            {
                action: 'addAll',
                label: '',
                className: 'btn btn-sm btn-block btn-success fas fa-angle-double-right'
            },
            {
                action: 'remove',
                label: '',
                className: 'btn btn-sm btn-block btn-danger fas fa-angle-left'
            },
            {
                action: 'removeAll',
                label: '',
                className: 'btn btn-sm btn-block btn-danger fas fa-angle-double-left'
            }
        ],

        label:
        {
            content: ['Rotas disponíveis:', 'Rotas autorizadas:']
        }
    });

    lista.on('picklist.add', function (event, rota)
    {
        associaRota(id_classe,rota,true)
    });

    lista.on('picklist.remove', function (event, rota)
    {
        associaRota(id_classe,rota,false)
    });
};

$('#cbRotaPadrao').selectpicker();

$('#cbClasse').change(function()
{
    if($(this).val() != '')
    {
        var vincula = $("#btnVincularRotas"), rota = $("#btnRotaPadrao");

        if(rota.hasClass('active'))
        {
            rota.removeClass('active');
        }

        if(!vincula.hasClass('active'))
        {
            vincula.addClass('active');
        }

        CarregaRotasClasse($(this).val(),$('#cbSistema').val());
        MostraOpcoes(true);
        MostraRotaPadrao(false);
    }
    else
    {
        MostraOpcoes(false);
        MostraRotaPadrao(false);
        MostraListasVincular(false);
    }
});

$('#cbRotaPadrao').change(function()
{
    if($(this).val() != '')
    {
        RotaPadraoClasse($('#cbClasse').val(),$(this).val());
    }
});


$("#btnRotaPadrao").click(function()
{
    var rota = $(this), vincula = $("#btnVincularRotas");

    if(!rota.hasClass('active'))
    {
        rota.addClass('active');
    }

    if(vincula.hasClass('active'))
    {
        vincula.removeClass('active');
    }

    MostraListasVincular(false);
    MostraRotaPadrao(true);
});

$("#btnVincularRotas").click(function()
{
    $('#cbClasse').trigger('change');

    MostraListasVincular(true);
    MostraRotaPadrao(false);
});

function MostraOpcoes(mostra)
{
    var opcoes = $("#divOpcoes");

    if(mostra)
    {
        if(opcoes.hasClass('d-none'))
        {
            opcoes.removeClass('d-none');
        }
    }
    else
    {
        if(!opcoes.hasClass('d-none'))
        {
            opcoes.addClass('d-none');
        }
    }
}

function MostraRotaPadrao(mostra)
{
    var div = $("#divRotaPadrao");

    if(mostra)
    {
        if(div.hasClass("d-none"))
        {
            div.removeClass("d-none");
        }

        CarregaRotasPadrao($("#cbClasse").val(),$('#cbSistema').val());
    }
    else
    {
        if(!div.hasClass("d-none"))
        {
            div.addClass("d-none");
        }
    }
}

function associaRota(id_classe,rota,adiciona)
{
    if(rota != null && rota.length > 0)
    {
        $.ajaxSetup(
        {
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax(
        {
            url: "ajax-associa-classe-rota",
            method: 'post',
            data:
            {
                rotas: rota,
                idClasse:id_classe,
                associa:adiciona,
            },
            success: function()
            {
                console.log('sucesso');
            },
            error:function(error)
            {
                MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
            }
        });
    }
}

function MostraListasVincular(mostra)
{
    if(mostra)
    {
        $('#colunaPagina').append('<div id="pick"></div>');
    }
    else
    {
        $('#colunaPagina div[id=pick]').remove();
    }
}

function CarregaRotasClasse(id_classe,id_sistema)
{
    var beforeSend = function()
        {
            MostraListasVincular(false);

            ModalAjaxCarregando(true,$("#corpoPagina"),'Carregando...');
        },
        complete = function()
        {
            ModalAjaxCarregando(false,$("#corpoPagina"),null);
        },
        sucesso = function(data)
        {
            MostraListasVincular(true);

            MostraListaRotas(id_classe,data);
        };

        BuscaRotas(id_classe,id_sistema,beforeSend,complete,sucesso);
}

function CarregaRotasPadrao(id_classe,id_sistema)
{
    var beforeSend = function()
    {
        $("#cbRotaPadrao").prop('disabled',false);

        $("#cbRotaPadrao option").remove();
        $("#cbRotaPadrao").append("<option>Carregando...</option>");
    },
    complete = function()
    {
        //ModalAjaxCarregando(false,$("#corpoPagina"),null);
    },
    sucesso = function(data)
    {
        $("#cbRotaPadrao option").remove();

        var lst = data['selected'];

        if(lst != null && lst.length > 0)
        {
            var padrao = null;

            lst.forEach(item =>
            {
                if(item.menu && item.id_pai != null)
                {
                    if(item.padrao)
                    {
                        padrao = item.id;
                    }

                    $("#cbRotaPadrao").append('<option value="'+ item.id +'">'+ item.label +'</option>');
                }
            });

            if(padrao != null)
            {
                $("#cbRotaPadrao").val(padrao);
            }
            else
            {
                $("#cbRotaPadrao").prepend('<option value="">Selecione</option>');
                $("#cbRotaPadrao").val('');
            }

            $("#cbRotaPadrao").selectpicker('refresh');
        }
        else
        {
            $("#cbRotaPadrao").append("<option>Nenhuma rota disponível</option>");
            $("#cbRotaPadrao").prop('disabled',true);
        }
        //console.log(lst);
    };

    BuscaRotas(id_classe,id_sistema,beforeSend,complete,sucesso);
}

function BuscaRotas(id_classe,id_sistema,before,complete,sucesso)
{
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "ajax-carrega-classe-rotas",
        method: 'post',
        data:
        {
            id: id_classe,
            id_sistema: id_sistema
        },
        beforeSend: before(),
        complete: complete(),
        success: function(data)
        {
            sucesso(data);
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}


function RotaPadraoClasse(id_classe,id_rota)
{
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "ajax-seta-rota-padrao",
        method: 'post',
        data:
        {
            classe: id_classe,
            rota: id_rota
        },
        success: function(data)
        {
            console.log(data);
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

$('#cbSistema').change(function()
{
    if($('#cbSistema').val() == '')
    {
        $('#cbClasse option').remove();
        $("#cbClasse").prepend('<option value="">Selecione uma Classe</option>');
        $("#cbClasse").prop('disabled',true);
    }
    else
    {
        CarregaClasses($('#cbSistema').val(),null);
    }
});

function LimparCampos()
{
    $('#id_form_modal').val('');
    
    $('#cbSistema').val('');
    $('#cbClasse').val('');

    $('#cbClasse option').remove();
    $("#cbClasse").prepend('<option value="">Selecione uma Classe</option>');
    $("#cbClasse").prop('disabled',true);
}

function CarregaClasses(id_sistema, id_classe)
{
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "ajax-lista-classe",
        method: 'post',
        data:
        {
            sistema: id_sistema
        },
        beforeSend: function()
        {
			$("#cbClasse option").remove();
        },
        complete: function()
        {
            
        },
        success: function(data)
        {
            if(!data.length){
                $('#cbClasse').append('<option value="">Nenhuma Classe</option>');
                $("#cbClasse").prop('disabled',false);
            }
            else
            {
                $('#cbClasse').append('<option value="">Selecione uma Classe</option>');
                data.forEach(item =>
                {
                    $("#cbClasse").append('<option value="' + item.id + '">' + item.descricao + '</option>');
                }); 
                $("#cbClasse").prop('disabled',false);
    
                if (id_classe != null)
                {
                    $("#cbClasse").val(id_classe);
                }
            } 
        
        },
        error:function(error)
        {
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}