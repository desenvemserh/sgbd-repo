$(document).ready(function(){
    ListaFuncionario();
});

$("#btnNovoUsuario").click(function()
{
    LimparCampos();

    $('#txtSenha_label').html('Senha');

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#btnPesquisaUsuario").click(function()
{
    PesquisaUsuario
    (
        $("#txtNomePesquisa").val(),
        $("#txtCpfPesquisa").val(),
        $("#txtTipoPesquisa").val(),
        $("#txtEmpresaPesquisa").val(),
        $("#txtStatusPesquisa").val()
    );
});

$("#tbUsuarios tbody").on("click","span[name=editBtn]",function()
{
    var id = $(this).attr("data");

    LimparCampos();

    $('#titulo_form_modal').text("Atualizar Usuário");
    $('#id_form_modal').val(id);
    $('#txtCpf').val($(".mask_cpf").masked($("#" + id + "_cpf").text()));
    $('#txtNome').val($("#" + id + "_nome").text());
    $('#cbFuncionario').val($('#' + id + '_funcionario').attr('data'));
    $('#txtEmail').val($('#' + id + '_email').text());
    $('#cbAtivo').prop('checked',$("#" + id + "_ativo").attr('data')==1);
    $('#txtSenha_label').html('Senha <b style="color:red">(Digitar uma nova senha substitui a atual)</b>');
    $('#txtSenha').prop('required',false);
    $('#txtConfirmSenha').prop('required',false);

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#tbUsuario tbody").on("click","span[name=delBtn]",function()
{
    var id = $(this).attr("data");

    $('#id_delete_modal').val(id);
    $('#item_delete').text($("#" + id + "_nome").text());
    $("#dlgDelete").modal({backdrop: "static"}).show();
});

$("#sldTerceiro").change(function()
{
    if(!($(this).prop('checked')))
    {
        $("#cbTerceiro").val('');
    }

    $("#cbTerceiro").prop('disabled',!($(this).prop('checked')));
    $("#txtNome").prop('disabled',!($(this).prop('checked')));
    $("#txtEmail").prop('disabled',!($(this).prop('checked')));
});



function LimparCampos()
{
    $('#titulo_form_modal').text("Novo Usuário");

    $('#sldTerceiro').prop('checked',true);
    $('#cbAtivo').prop('checked',true);

    $("#cbTerceiro").prop('disabled',false);
    $("#txtNome").prop('disabled',false);
    $("#txtEmail").prop('disabled',false);

    $('#id_form_modal').val('');
	$('#txtNome').val('');
    $('#txtEndereco').val('');
    $('#txtCpf').val('');
    $('#txtEmail').val('');
    $('#cbFuncionario').val('');
    $('#txtSenha_label').html('');
}

function PesquisaUsuario(nomePesq,cpfPesq,tipoPesq,empresaPesq,statusPesq)
{
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "ajax-pesquisa-usuario",
        method: 'post',
        data:
        {
            nome: nomePesq,
            cpf: cpfPesq,
            tipo: tipoPesq,
            empresa: empresaPesq,
            status: statusPesq
        },
        beforeSend: function()
        {
			$("#tbUsuario tbody tr").remove();
            
            ModalAjaxCarregando(true,$("#pgUsuario"),'Carregando...');
        },
        complete: function()
        {
            ModalAjaxCarregando(false,$("#pgUsuario"),null);
        },
        success: function(data)
        {
            $("#tbUsuario").html(data);

            /*
            data.forEach(item =>
            {
                $("#tbUsuario tbody").append(linhaTabelaUsuario(item));
            });
            */
        },
        error:function(error)
        {
            console.log(error);

            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function linhaTabelaUsuario(item)
{
    var linha = '<tr>'
              + '<td id="'+ item.id +'_usuario">'+ item.nome +'</td>'
              + '<td id="'+ item.id +'_cpf">'+ item.cpf +'</td>'
              + '<td id="'+ item.id +'_tipo">'+ item.tipo +'</td>'
              + '<td id="'+ item.id +'_classe" data="'+ item.classe.id +'">'+ item.classe.codigo +'</td>'
              + '<td id="'+ item.id +'_terceiro" data="'+ item.terceiro.id +'">'+ item.terceiro.descricao +'</td>'
              + '<td id="'+ item.id +'_ativo" data="'+ item.ativo +'">';
    
    if(item.ativo)
    {
        linha += '<span class="badge badge-success">Ativo</span>';
    }
    else
    {
        linha += '<span class="badge badge-danger">Inativo</span>';
    }
    
    linha += '</td>'
          +  '<td>'
          +    '<span name="editBtn" data="'+ item.id +'" class="fas fa-edit botaoeditar cursor-pointer" title="Editar"></span>'
          +    '<span name="delBtn" data="'+ item.id +'" class="fas fa-trash-alt botaoexcluir cursor-pointer" title="Excluir"></span>'
          +  '</td>'
          +'</tr>';
}

function ListaFuncionario(id_chamado, id_funcionario)
{
   
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "ajax-lista-funcionario",
        method: 'post',
        dataType: "json",
        data:
        {
           chamado: id_chamado
        },
        beforeSend: function()
        {
            
        },
        complete: function()
        {
            $('#cbFuncionario').prop('disabled',false);
        },
        success: function(data)
        {
       
            data.consulta.forEach(item =>
            {
                $('#cbFuncionario').append('<option value="' + item.id + '">' + item.nome + '</option>');
                
            });

            if (id_funcionario != null)
            {
                $("#cbFuncionario").val(id_funcionario);
            }

        },
        error:function(error)
        {
            console.log(error);

            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });

}