$('#tbPermissao').DataTable(
    {
        paginate:true,
        info: false,
        searching:true,
        retrieve: true,
        language:
        {
            lengthMenu: "Mostrar _MENU_ registros por página",
            emptyTable: "Nenhum resultado encontrado",
            decimal:",",
            info:"Mostrando _START_ a _END_ de _TOTAL_ registros",
            infoEmpty:"Mostrando 0 de 0 registros",
            infoFiltered:"(filtrado de _MAX_ do total de registros)",
            paginate:
            {
                first:"Primeiro",
                last:"Último",
                next:"Próximo",
                previous:"Anterior"
            },
            thousands:".",
            zeroRecords:"Nenhum registro encontrado",
            search:"Pesquisar:",
            loadingRecords:"Carregando...",
            processing:"Processando...",
        }
});

$(document).ready(function()
{
    $('#btnNovoParametro').click(function()
    {
        LimparCampos();

        $('#titulo_form_modal').text("Nova Permissão");

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });


    $("#tbPermissao tbody").on("click","span[name=editBtn]",function()
    {
        LimparCampos();
        
        $('#titulo_form_modal').text("Atualizar Permissão");

        var id = $(this).attr("data");

        $('#id_form_modal').val(id);
        $('#cbUsuario').val($('#' + id + '_fk_usuario').attr('data'));
        $('#cbSistema').val($('#' + id + '_sistema').attr('data'));
        CarregaClasses($('#cbSistema').val(), $('#' + id + '_fk_classe').attr('data'));

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });


    $("#tbPermissao tbody").on("click","span[name=delBtn]",function()
    {
        var id = $(this).attr("data");

        $('#id_delete_modal').val(id);
        // $('#item_delete').text($("#" + id + "_codigo").text());
        $("#dlgDelete").modal({backdrop: "static"}).show();
    });
});

$('#cbSistema').change(function()
{
    if($('#cbSistema').val() == '')
    {
        $('#cbClasse option').remove();
        $("#cbClasse").prepend('<option value="">Selecione uma Classe</option>');
        $("#cbClasse").prop('disabled',true);
    }
    else
    {
        CarregaClasses($('#cbSistema').val(),null);
    }
});

function LimparCampos()
{
    $('#id_form_modal').val('');
    $('#txtCodigo').val('');
    $('#txtDescricao').val('');
    $('#txtValor').val('');
    
    $('#cbUsuario').val('');
    $('#cbSistema').val('');
    $('#cbClasse').val('');

    $('#cbClasse option').remove();
    $("#cbClasse").prepend('<option value="">Selecione uma Classe</option>');
    $("#cbClasse").prop('disabled',true);
}

function CarregaClasses(id_sistema, id_classe)
{
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "ajax-lista-classe",
        method: 'post',
        data:
        {
            sistema: id_sistema
        },
        beforeSend: function()
        {
			$("#cbClasse option").remove();
        },
        complete: function()
        {
            
        },
        success: function(data)
        {
            if(!data.length){
                $('#cbClasse').append('<option value="">Nenhuma Classe</option>');
                $("#cbClasse").prop('disabled',false);
            }
            else
            {
                data.forEach(item =>
                {
                    $("#cbClasse").append('<option value="' + item.id + '">' + item.descricao + '</option>');
                }); 
                $("#cbClasse").prop('disabled',false);
    
                if (id_classe != null)
                {
                    $("#cbClasse").val(id_classe);
                }
            } 
        
        },
        error:function(error)
        {
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}