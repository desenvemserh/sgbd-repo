$('#tbClasses').DataTable(
    {
        paginate:true,
        info: false,
        searching:true,
        retrieve: true,
        language:
        {
            lengthMenu: "Mostrar _MENU_ registros por página",
            emptyTable: "Nenhum resultado encontrado",
            decimal:",",
            info:"Mostrando _START_ a _END_ de _TOTAL_ registros",
            infoEmpty:"Mostrando 0 de 0 registros",
            infoFiltered:"(filtrado de _MAX_ do total de registros)",
            paginate:
            {
                first:"Primeiro",
                last:"Último",
                next:"Próximo",
                previous:"Anterior"
            },
            thousands:".",
            zeroRecords:"Nenhum registro encontrado",
            search:"Pesquisar:",
            loadingRecords:"Carregando...",
            processing:"Processando...",
        }
});

$(document).ready(function()
{
    $('#btnNovaClasse').click(function()
    {
        LimparCampos();
        
        $('#titulo_form_modal').text("Nova Classe");

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });


    $("#tbClasses tbody").on("click","span[name=editBtn]",function()
    {
        LimparCampos();
        
        $('#titulo_form_modal').text("Atualizar Classe");

        var id = $(this).attr("data");

        $('#id_form_modal').val(id);
        $('#txtCodigo').val($("#" + id + "_codigo").text());
        $('#txtDescricao').val($("#" + id + "_descricao").text());
        $('#cbSistema').val($('#' + id + '_sistema').attr('data'));
        $('#cbAdmin').prop('checked',$("#" + id + "_admin").attr('data')==1);

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });


    $("#tbClasses tbody").on("click","span[name=delBtn]",function()
    {
        var id = $(this).attr("data");

        $('#id_delete_modal').val(id);
        $('#item_delete').text($("#" + id + "_codigo").text());
        $("#dlgDelete").modal({backdrop: "static"}).show();
    });
});

function LimparCampos()
{
    $('#id_form_modal').val('');
    $('#txtCodigo').val('');
    $('#txtDescricao').val(''); 
    $('#cbSistema').val('');
}