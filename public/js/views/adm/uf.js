$('#tbEstados').DataTable(
    {
        paginate:true,
        info: false,
        searching:true,
        retrieve: true,
        language:
        {
            lengthMenu: "Mostrar _MENU_ registros por página",
            emptyTable: "Nenhum resultado encontrado",
            decimal:",",
            info:"Mostrando _START_ a _END_ de _TOTAL_ registros",
            infoEmpty:"Mostrando 0 de 0 registros",
            infoFiltered:"(filtrado de _MAX_ do total de registros)",
            paginate:
            {
                first:"Primeiro",
                last:"Último",
                next:"Próximo",
                previous:"Anterior"
            },
            thousands:".",
            zeroRecords:"Nenhum registro encontrado",
            search:"Pesquisar:",
            loadingRecords:"Carregando...",
            processing:"Processando...",
        }
});

$(document).ready(function()
{
    $("#tbEstados tbody").on("click","span[name=editBtn]",function()
    {
        $('#titulo_form_modal').text("Atualizar Estado");

        var id = $(this).attr("data");

        $('#id_form_modal').val(id);
        $('#txtNome').val($("#" + id + "_nome").text());
        $('#txtSigla').val($("#" + id + "_sigla").text());

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });
});