$(function(){
    // ConstroiCharts();
    var id = $('#hdnProjeto').val();
    carregaGrafico(id);
});
function ConstroiCharts(dados)
{
    Highcharts.chart('container', {
        chart: {
            type: 'xrange'
        },
        title: {
            text: 'Cronograma'
        },
        accessibility: {
            point: {
                descriptionFormatter: function (point) {
                    var ix = point.index + 1,
                        category = point.yCategory,
                        from = new Date(point.x),
                        to = new Date(point.x2);
                    return ix + '. ' + category + ', ' + from.toDateString() +
                        ' to ' + to.toDateString() + '.';
                }
            }
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: ''
            },
            categories: ['A fazer', 'Execução', 'Feito'],
            reversed: true
        },
        series: [{
            name: dados[3],
            // pointPadding: 0,
            // groupPadding: 0,
            borderColor: 'gray',
            pointWidth: 20,
            data: [{
                x: Date.UTC(dados[0][0], dados[0][1], dados[0][2]),
                x2: Date.UTC(dados[0][3], dados[0][4], dados[0][5]),
                y: 0,
            }, {
                x: Date.UTC(dados[1][0], dados[1][1], dados[1][2]),
                x2: Date.UTC(dados[1][3], dados[1][4], dados[1][5]),
                y: 1
            }, {
                x: Date.UTC(dados[2][0], dados[2][1], dados[2][2]),
                x2: Date.UTC(dados[2][3], dados[2][4], dados[2][5]),
                y: 2,
                // partialFill: 0.25
            }],
            dataLabels: {
                enabled: true
            }
        }]
    });
}

//AJAX
function carregaGrafico(id)
{
    $.ajax(
    {
        url: "../ajax-resultado",
        method: 'get',
        data:
        {
            sistema: id
        },
        success: function(data)
        {
            ConstroiCharts(data);
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}