$(document).ready(function(){
    PesquisaAFazer($('#hdnId').val(),0);
    PesquisaEmExecucao($('#hdnId').val(),1);
    PesquisaFeito($('#hdnId').val(),2);
});

$('#btnAdicionarTarefa').click(function()
{
    LimparCampos();
    
    $('#titulo_form_modal').text("Nova Tarefa");
    $('#hdnProj').val($('#hdnId').val());
    $('#hdnProgress').val($('#hdnProgresso').val());

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$('#addSubTarefas').click(function()
{
    var titulo = $("#txtTituloSub"),
        descricao = $('#txtDescricaoSub');

    if(titulo.val() == '')
    {
        if(!titulo.hasClass('border-danger'))
        {
            titulo.addClass('border-danger');
        }

        Popover(titulo,'É necessário informar um título');

        return;
    }
    else if(descricao.val() == '')
    {
        if(!descricao.hasClass('border-danger'))
        {
            descricao.addClass('border-danger');
        }

        Popover(descricao,'É necessário informar uma descrição');

        return;
    }

    var id = $('#hdnSubtarefa').val(),
        tarefa = $('#id_form_modal_dlgEdicao').val()
        titulo = $('#txtTituloSub').val(),
        descricao = $('#txtDescricaoSub').val(),
        status = $('#cbStatusSub').val();

    adicionarSubtarefa(id,tarefa,titulo,descricao,status);
    
    $('#txtTituloSub').val(''),
    $('#txtDescricaoSub').val(''),
    $('#cbStatusSub').val('');
});

$("#tbPendente tbody").on("click","span[name=editBtn]",function()
{
    LimparCampos();
    
    $('#titulo_form_modal_dlgEdicao').text("Atualizar Tarefa");

    var id = $(this).attr("data");
    PesquisaTarefa(id);
    PesquisaSubtarefa(id);

    $('#id_form_modal_dlgEdicao').val(id);

    $("#dlgEdicao").modal({backdrop: "static"}).show();
});

$("#tbExecucao tbody").on("click","span[name=editBtn]",function()
{
    LimparCampos();
    
    $('#titulo_form_modal_dlgEdicao').text("Atualizar Tarefa");

    var id = $(this).attr("data");
    PesquisaTarefa(id);
    PesquisaSubtarefa(id);

    $('#id_form_modal_dlgEdicao').val(id);

    $("#dlgEdicao").modal({backdrop: "static"}).show();
});

$("#tbFeito tbody").on("click","span[name=editBtn]",function()
{
    LimparCampos();
    
    $('#titulo_form_modal_dlgEdicao').text("Atualizar Tarefa");

    var id = $(this).attr("data");
    PesquisaTarefa(id);
    PesquisaSubtarefa(id);

    $('#id_form_modal_dlgEdicao').val(id);

    $("#dlgEdicao").modal({backdrop: "static"}).show();
});

$("#tbSubtarefas tbody").on("click","span[name=editBtn]",function()
{
    var id = $(this).attr("data");

    finalizarSubtarefa(id);
});

$("#tbSubtarefas tbody").on("click","span[name=excluirBtn]",function()
{
    var id = $(this).attr("data");

    excluirSubtarefa(id);
});

function LimparCampos()
{
    $('#id_form_modal').val('');
    $("#txtTitulo").val('');
    $("#txtDescricao").val('');
    $("#txtPeso").val('');
    $("#cbDesenvolvedor").val('');
}

function PesquisaAFazer(projeto, status)
{
   
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "../ajax-tarefa-afazer",
        method: 'post',
        data:
        {
            projeto: projeto,
            status: status
        },
        beforeSend: function()
        {
            $('#tbPendente tbody tr').remove();
            // ModalAjaxCarregando(true,$("#lista"),'Pesquisando...');
        },
        complete: function()
        {
            // ModalAjaxCarregando(false,$("#lista"),null);
        },
        success: function(data)
        {
            if(data.length > 0)
            {

                data.forEach(item =>
                {
                    // var titulo = item.titulo.Value;
                    // console.log(item.titulo.replace(/'/g, "\\'"));
                    var peso = '';
                    if(item.peso=='1')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='2')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='3')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='4')
                    {
                        peso=' <span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='5')
                    {
                        peso='<span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='6')
                    {
                        peso='<span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='7')
                    {
                        peso=' <span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='8')
                    {
                        peso='<span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }
                    else if(item.peso=='9')
                    {
                        peso='<span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }
                    else if(item.peso=='10')
                    {
                        peso=' <span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }
                   
                    var linha = '<tr id="'+item.id+'" ondragstart="drag(event)" draggable="true">'
                            +      '<td id="'+ item.id +'_tarefa"><b>'+ item.titulo + '</b>'
                            +      '<br/><br/>'+ peso + '<br/><br/><img class="img-circle" src="../../'+item.foto +'" height="30"/>&nbsp&nbsp'+item.dataCriacao+'</td>'
                            
                            +      '<td width="10%" class="text-center"><br/><br/>'
                            +         '<span name="editBtn" class="fas fa-edit fa-lg cursor-pointer botaoeditar" data="'+ item.id +'" title="Editar a Fazer"></span>'
                            +      '</td>'
                            +   '</tr>';

                        $('#tbPendente tbody').append(linha);
                });
            }
            else
            {
                var linhanada = '<tr><td><i class="fas fa-exclamation-triangle"></i> Nenhuma tarefa encontrada !</td><td></td></tr>';
                $("#tbPendente tbody").append(linhanada);
            }
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function PesquisaEmExecucao(projeto, status)
{
   
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "../ajax-tarefa-emexecucao",
        method: 'post',
        data:
        {
            projeto: projeto,
            status: status
        },
        beforeSend: function()
        {
            $('#tbExecucao tbody tr').remove();
            // ModalAjaxCarregando(true,$("#lista"),'Pesquisando...');
        },
        complete: function()
        {
            // ModalAjaxCarregando(false,$("#lista"),null);
        },
        success: function(data)
        {
            if(data.length > 0)
            {

                data.forEach(item =>
                {
                    var peso = '';
                    if(item.peso=='1')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='2')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='3')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='4')
                    {
                        peso=' <span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='5')
                    {
                        peso='<span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='6')
                    {
                        peso='<span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='7')
                    {
                        peso=' <span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='8')
                    {
                        peso='<span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }
                    else if(item.peso=='9')
                    {
                        peso='<span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }
                    else if(item.peso=='10')
                    {
                        peso=' <span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }

                    var linha = '<tr id="'+item.id+'" ondragstart="drag(event)" draggable="true">'
                    +      '<td id="'+ item.id +'_tarefa"><b>'+ item.titulo + '</b>'
                    +      '<br/><br/>'+ peso + '<br/><br/><img class="img-circle" src="../../'+item.foto +'" height="30"/>&nbsp&nbsp'+item.dataCriacao+'</td>' 

                    +      '<td width="10%" class="text-center"><br/><br/>'
                    +         '<span name="editBtn" class="fas fa-edit fa-lg cursor-pointer botaoeditar" data="'+ item.id +'" title="Editar a Fazer"></span>'
                    +      '</td>'
                    +   '</tr>';

                        $('#tbExecucao tbody').append(linha);
                });
            }
            else
            {
                var linhanada = '<tr><td><i class="fas fa-exclamation-triangle"></i> Nenhuma tarefa encontrada !</td><td></td></tr>';
                $("#tbExecucao tbody").append(linhanada);
            }
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function PesquisaFeito(projeto, status)
{
   
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "../ajax-tarefa-feito",
        method: 'post',
        data:
        {
            projeto: projeto,
            status: status
        },
        beforeSend: function()
        {
            $('#tbFeito tbody tr').remove();
            // ModalAjaxCarregando(true,$("#lista"),'Pesquisando...');
        },
        complete: function()
        {
            // ModalAjaxCarregando(false,$("#lista"),null);
        },
        success: function(data)
        {
            if(data.length > 0)
            {

                data.forEach(item =>
                {
                   var peso = '';
                    if(item.peso=='1')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='2')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='3')
                    {
                        peso='<span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='4')
                    {
                        peso=' <span class="badge badge-pill badge-info">BAIXO</span>';
                    }
                    else if(item.peso=='5')
                    {
                        peso='<span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='6')
                    {
                        peso='<span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='7')
                    {
                        peso=' <span class="badge badge-pill badge-warning">MÉDIO</span>';
                    }
                    else if(item.peso=='8')
                    {
                        peso='<span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }
                    else if(item.peso=='9')
                    {
                        peso='<span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }
                    else if(item.peso=='10')
                    {
                        peso=' <span class="badge badge-pill badge-danger">DIFÍCIL</span>';
                    }

                    var linha = '<tr id="'+item.id+'" ondragstart="drag(event)" draggable="true">'
                    +      '<td id="'+ item.id +'_tarefa"><b>'+ item.titulo + '</b>'
                    +      '<br/><br/>'+ peso + '<br/><br/><img class="img-circle" src="../../'+item.foto +'" height="30"/>&nbsp&nbsp'+item.dataCriacao+'</td>'
                    
                    +      '<td width="10%" class="text-center"><br/><br/>'
                    +         '<span name="editBtn" class="fas fa-edit fa-lg cursor-pointer botaoeditar" data="'+ item.id +'" title="Editar a Fazer"></span>'
                    +      '</td>'
                    +   '</tr>';

                        $('#tbFeito tbody').append(linha);
                });
            }
            else
            {
                var linhanada = '<tr><td><i class="fas fa-exclamation-triangle"></i> Nenhuma tarefa encontrada !</td><td></td></tr>';
                $("#tbFeito tbody").append(linhanada);
            }
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}


function PesquisaTarefa(tarefa)
{
   
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "../ajax-pesquisa",
        method: 'post',
        data:
        {
            tarefa: tarefa
        },
        beforeSend: function()
        {
            $('#spanStatus').removeClass('badge badge-pill badge-info badge-secondary badge-success text-center');
        },
        complete: function()
        {
           
        },
        success: function(data)
        {
            if(data.length > 0)
            {

                data.forEach(item =>
                {
                    // console.log(data);
                    $('#hdnProjE').val($('#hdnId').val());
                    $('#hdnProgressE').val($('#hdnProgresso').val());
                    $('#cbDesenvolvedorE').val(item.desenvolvedor);
                    $('#txtTituloE').val(item.titulo);
                    $('#txtDescricaoE').val(item.descricao);
                    $('#txtPesoE').val(item.peso);
                    $('#cbStatusE').val(item.status);
                    $('#txtDataInicioE').val(item.dataInicio);
                    $('#txtDataFimE').val(item.dataFim);
                });
            }
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function dropP(ev){
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    document.getElementById("addTarefaP").appendChild(document.getElementById(data));

    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: '../../projeto/tarefa/atualizar',
        method: 'POST',
        data:{
            id: data,
            status: '0'
        },
        beforeSend: function(){},
        complete: function(){},
        success: function()
        {
            
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function dropE(ev){
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    document.getElementById("addTarefaE").appendChild(document.getElementById(data));

    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: '../../projeto/tarefa/atualizar',
        method: 'POST',
        data:{
            id: data,
            status: '1'
        },
        beforeSend: function(){},
        complete: function(){},
        success: function()
        {
            
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function dropF(ev){
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    document.getElementById("addTarefaF").appendChild(document.getElementById(data));

    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: '../../projeto/tarefa/atualizar',
        method: 'POST',
        data:{
            id: data,
            status: '2'
        },
        beforeSend: function(){},
        complete: function(){},
        success: function()
        {
            
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function adicionarSubtarefa(id,tarefa,titulo,descricao,status){
    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: '../ajax-adicionar-subtarefa',
        method: 'POST',
        data:{
            id: id,
            tarefa: tarefa,
            titulo: titulo,
            descricao: descricao,
            status: status
        },
        beforeSend: function(){},
        complete: function(){},
        success: function()
        {
            PesquisaSubtarefa(tarefa);
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function PesquisaSubtarefa(tarefa)
{
   
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "../ajax-listar-subtarefa",
        method: 'post',
        data:
        {
            tarefa: tarefa
        },
        beforeSend: function()
        {
            $('#tbSubtarefas tbody tr').remove();
        },
        complete: function()
        {
           
        },
        success: function(data)
        {
            if(data.length > 0)
            {

                data.forEach(item =>
                {
                    var status = '';
                    if(item.status=='0')
                    {
                        status='<span class="badge badge-pill badge-warning">A Fazer</span>';
                    }
                    else if(item.status=='1')
                    {
                        status='<span class="badge badge-pill badge-success">Feito</span>';
                    }
                    
                    var linha = '<tr>'
                    +      '<td id="'+ item.id +'_titulo" class="text-center"><b>'+ item.titulo + '</b></td>'
                    +      '<td id="'+ item.id +'_descricao"><b>'+ item.descricao + '</b></td>'
                    +      '<td id="'+ item.id +'_status" class="text-center" data="'+ item.status +'"><b>'+ status + '</b></td>'
                    
                    +      '<input id="' + item.id + '_id" type="hidden" data="'+ item.id +'"/>'
                    +      '<td width="10%" class="text-center">'
                    +           (item.status == '0' ?  '<span name="editBtn" class="fas fa-check-circle fa-lg cursor-pointer" data="'+ item.id +'" title="Editar"></span>' : '')
                +               '<span name="excluirBtn" class="fas fa-trash-alt fa-lg cursor-pointer" data="'+ item.id +'" title="Excluir"></span>'
                    +      '</td>'
                    +   '</tr>';

                        $('#tbSubtarefas tbody').append(linha);
                });
            }
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function excluirSubtarefa(id){
    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: '../ajax-excluir-subtarefa',
        method: 'POST',
        data:{
            subtarefa: id
        },
        beforeSend: function(){},
        complete: function(){},
        success: function()
        {
            PesquisaSubtarefa($('#id_form_modal_dlgEdicao').val());
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function finalizarSubtarefa(id){
    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: '../ajax-finalizar-subtarefa',
        method: 'POST',
        data:{
            subtarefa: id
        },
        beforeSend: function(){},
        complete: function(){},
        success: function()
        {
            PesquisaSubtarefa($('#id_form_modal_dlgEdicao').val());
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}