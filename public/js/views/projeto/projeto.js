$(document).ready(function()
{
    $('#btnNovoProjeto').click(function()
    {
        LimparCampos();
        
        $('#titulo_form_modal').text("Novo Projeto");

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });

    $("#tbProjeto tbody").on("click","span[name=editBtn]",function()
    {
        LimparCampos();
        
        $('#titulo_form_modal').text("Atualizar Projeto");

        var id = $(this).attr("data");

        $('#id_form_modal').val(id);
        $('#cbSistema').val($("#" + id + "_sistema").attr('data'));

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });

    $("#tbProjeto tbody").on("click","span[name=tarefaBtn]",function()
    {
        LimparCampos();
        
        $('#titulo_form_modal_dlgTarefa').text("Adicionar Tarefa");

        var id = $(this).attr("data");

        $('#id_form_modal_dlgTarefa').val(id);

        CarregaTarefa(id);

        $("#dlgTarefa").modal({backdrop: "static"}).show();
    });
});

function LimparCampos()
{
    $('#id_form_modal').val(''); 
    $('#cbSistema').val('');
    $('#cbSistemaT').val('');
}

$("#btnSalvarTarefa").click(function()
{
    var titulo = $("#txtTitulo"),
        descricao = $('#txtDescricao');

    if(titulo.val() == '')
    {
        if(!titulo.hasClass('border-danger'))
        {
            titulo.addClass('border-danger');
        }

        Popover(titulo,'É necessário informar um título');

        return;
    }
    else if(descricao.val() == '')
    {
        if(!descricao.hasClass('border-danger'))
        {
            descricao.addClass('border-danger');
        }

        Popover(comentario,'É necessário informar uma descrição');

        return;
    }

    var id_projeto = $('#id_form_modal_dlgTarefa').val(),
        titulo = $('#txtTitulo').val(),
        descricao = $("#txtDescricao").val(),
        peso = $('#txtPeso').val(),
        desenvolvedor = $('#cbDesenvolvedor').val();

    //executa o método de inserir histórico
    InsereTarefa(id_projeto,titulo,descricao,peso,desenvolvedor,nivel=null);
   
    CarregaTarefa($('#id_form_modal_dlgTarefa').val());
    // //fecha a modal
    // $("#dlgComentario").modal('toggle');
});

function InsereTarefa(projeto,titulo,descricao,peso,desenvolvedor,nivel=null)
{
    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: "../../projeto/ajax-insere-tarefa",
        method: 'post',
        data:
        {
            projeto:projeto,
            titulo: titulo,
            descricao: descricao,
            peso: peso,
            desenvolvedor: desenvolvedor
        },
        beforeSend: function()
        {
            // ModalAjaxCarregandoImg(true,$("#dlgComentario"));
        },
        complete: function()
        {
            // ModalAjaxCarregandoImg(false,$("#dlgComentario"));
            ModalAjaxCarregando(false,$("#dlgTarefa"),null);
        },
        success: function(data)
        {
            metodo_sucesso(data);
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

/**
 * Carrega e Insere Lista de Atividades da O.S
 */
function CarregaTarefa(id_projeto = 0)
{
    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

    $.ajax(
    {
        url: "../../projeto/ajax-lista-tarefa",
        method: 'post',
        data:
        {
            id_projeto: id_projeto
        },
        beforeSend: function()
        {
            ZeraLinhasTarefa();
        },
        success: function(data)
        {
            if(data.length > 0)
            {
                data.forEach(item =>
                {
                    InsereLinhaTarefa(item.projeto,item.titulo,item.descricao,item.desenvolvedor,item.status);
                });
            }
        },
        error:function(error)
        {
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function LimparCampoTarefa()
{
    $("#txtTitulo").val('');
    $("#txtDescricao").val('');
    $("#txtPeso").val('');
    $("#cbDesenvolvedor").val('');
}

function ZeraLinhasTarefa()
{
    $("#tbTarefa tbody tr").remove();
}

function InsereLinhaTarefa(projeto,titulo,descricao,desenvolvedor,status)
{
    var linha =  $('#tbTarefa tbody tr').length;
    $("#tbTarefa").append
    (
        '<tr>'
        +   '<td>'+ titulo +'</td>'
        +   '<td>'+ descricao +'</td>'
        +   '<td>'+ desenvolvedor +'</td>'
        +    (status != 0 ? '<td class="font-weight-bold text-center"><h6><span class="badge badge-pill badge-info">Aberto</span></h6></td>' : '<td class="font-weight-bold text-center"><h6><span class="badge badge-pill badge-success">Finalizado</span></h6></td>')
        // +   '<td class="text-center"><span class="fas fa-trash text-danger cursor-pointer"></span></td>'
        // +   '<input type="hidden" name="atividade['+ linha +'][descricao]" value="'+ descricao +'" />'
        // +   '<input type="hidden" name="atividade['+ linha +'][ordem]" value="'+ ordem +'" />'
        // +   '<input type="hidden" name="atividade['+ linha +'][id]" value="'+ id +'" />'
        + '</tr>'
    );

    LimparCampoTarefa();
}