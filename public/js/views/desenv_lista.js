
$("#tbProjeto tbody").on("click","span[name=lstDesenvolvedorBtn]",function()
{
    var id = $(this).attr("data");

    CarregaDesenvolvedorByProjeto(id);

    // $('#mdlListarDesenvolvedor .modal-header').css('background-color', '#FF3300');

    $("#mdlListarDesenvolvedor").modal({backdrop: "static"}).show();
});


function MostraListaDesenvolvedor(id_projeto,dados)
{
   lista = $('#pick').pickList(
    {
        data: dados,
        buttons:
        [
            {
                action: 'add',
                label: '',
                className: 'btn btn-sm btn-block btn-success fas fas fa-angle-right'
            },
            {
                action: 'addAll',
                label: '',
                className: 'btn btn-sm btn-block btn-success fas fa-angle-double-right'
            },
            {
                action: 'remove',
                label: '',
                className: 'btn btn-sm btn-block btn-danger fas fa-angle-left'
            },
            {
                action: 'removeAll',
                label: '',
                className: 'btn btn-sm btn-block btn-danger fas fa-angle-double-left'
            }
        ],

        label:
        {
            content: ['Desenvolvedores disponíveis:', 'Desenvolvedores vinculados:']
        }
    });

    lista.on('picklist.add', function (event, usuario)
    {
        associaDesenvolvedor(id_projeto,usuario,true)
    });

    lista.on('picklist.remove', function (event, usuario)
    {
        associaDesenvolvedor(id_projeto,usuario,false)
    });
};


function CarregaDesenvolvedorByProjeto(id_projeto)
{
    $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
    {
        url: UrlBase("../ajax-carrega-desenvolvedor-by-projeto"),
        method: 'post',
        data:
        {
            id: id_projeto
        },
        beforeSend: function()
        {
            $('#divListaDesenvolvedorProjeto div[id=pick]').remove();

            ModalAjaxCarregando(true,$("#divListaDesenvolvedorProjeto"),'Carregando...');
        },
        complete: function()
        {
            ModalAjaxCarregando(false,$("#divListaDesenvolvedorProjeto"),null);
        },
        success: function(data)
        {
            $('#divListaDesenvolvedorProjeto').append('<div id="pick"></div>');

            MostraListaDesenvolvedor(id_projeto,data);
        },
        error:function(error)
        {
            console.log(error);
            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}


function associaDesenvolvedor(id_projeto,usuarios,adiciona)
{
    if(usuarios != null && usuarios.length > 0)
    {
        $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax(
        {
            url: UrlBase("../ajax-associa-desenvolvedor-projeto"),
            method: 'post',
            data:
            {
                usuarios: usuarios,
                idProjeto:id_projeto,
                associa:adiciona,
            },
            success: function()
            {
                //console.log('sucesso')
            },
            error:function(error)
            {
                MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
            }
        });
    }
}
//fecha a modal do comentário
$("#btnCancelarModal_mdlListarDesenvolvedor").click(function()
{
    window.location.reload();
    // //fecha a modal
    // $("#mdlListarDesenvolvedor").modal('toggle');
});