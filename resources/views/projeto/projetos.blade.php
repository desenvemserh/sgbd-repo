@extends('layouts.app')

@section('titulo')
    <i class="fas fa-key"></i> Projetos
@endsection

@section('css')
    <link rel="stylesheet" href="{{{ URL::asset('js/DataTables/datatables.min.css') }}}" />
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-picklist/picklist.css') }}" />
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/DataTables/datatables.min.js') }}}"></script>
    <script src="{{{ URL::asset('js/jquery-picklist/picklist.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/projeto/projeto.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/desenv_lista.js') }}}"></script>
@endsection

@section('pagina')
    
    <div class="container-fluid">
        <div class="card rounded text-left">
            <div class="card-body">
                <div class="row" >
                    <div class="col text-right">
                        <button id="btnNovoProjeto" type="button" class="btn btn-sm btn-primary" aria-label="Left Align" >
                            <i class="fas fa-plus-circle"></i> Novo Projeto
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <div class="row" >
                    <div class="col panel panel-primary table-responsive">
                        <table id="tbProjeto" class="table table-hover text-left">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Sistema</th>
                                    <th class="text-center">Progresso</th>
                                    <th class="text-center">Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($projetos as $projeto)
                                    <tr>
                                        <td class="text-center">{{$i}}</td>
                                        <td id="{{$projeto->getId()}}_sistema" data="{{$projeto->getFkSistema()}}">{{$projeto->Sistema->getDescricao()}}</td>
                                        <td id="{{$projeto->getId()}}_progresso" class="text-center">{{$projeto->getProgresso()}} %</td>
                                        <td class="text-center">
                                            <span id="spnStatus" status="{{$projeto->getStatus()}}"
                                                class="cursor-pointer badge badge-{{STATUS_PROJETO::GetBadge($projeto->getStatus())}}">
                                                {{STATUS_PROJETO::GetTipo($projeto->getStatus())}}
                                            </span>
                                        </td>
                                        <td width="10%" class="text-center">
                                            <div class="btn-group btn-group-sm mr-2" role="group" aria-label="First group">
                                                <span name="lstDesenvolvedorBtn" data="{{$projeto->getId()}}" class="btn btn-sm btn-info fas fa-th-list text-white cursor-pointer" title="ver desenvolvedores"></span>&nbsp;
                                                @if($projeto->ProjetoId() != '')
                                                <a href="{{route('projeto.tarefa',[Util::StringAleatoria(),$projeto->getId(),Util::StringAleatoria()])}}">
                                                    <span class="fas fa-eye btn btn-sm btn-info cursor-pointer text-white" title="Ver tarefas do projeto"></span>
                                                </a>
                                                @endif
                                            </div>
                                        </td>
                                        <input type="hidden" value="{{$i++}}"/>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Novo Projeto',
		'icone'=>'fa-cog',
        'rota'=>'projeto.projeto.salvar',
        'campos'=> 
        [
            [
                'id' =>'cbSistema',
                'label'=>'Sistema',
                'nome'=>'sistema',
                'tipo'=>'combo',
                'opcoes'=>$sistema,
                'required'=> true,
                'disabled'=>false,
                'default'=>'Selecione um Sistema'
            ],
        ]
    ])

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'idModal'=>'dlgTarefa',
        'titulo'=> 'Nova Tarefa',
        'icone'=>'fa-cog',
        'largura'=>'1000',
       
        'campos'=> 
        [
            [
                'tipo'=>'array',
                'campos'=> 
                [
                    [
                        'largura'=>'10',
                        'id' =>'txtTitulo',
                        'label'=>'Título',
                        'nome'=>'titulo',
                        'tipo'=>'txt',
                        'tamanho'=>'50',    
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'2',
                        'id' =>'txtPeso',
                        'label'=>'Peso',
                        'nome'=>'peso',
                        'tipo'=>'numero',
                        'tamanho'=>'2',
                        'min'=>'1',
                        'max'=>'10',    
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                ]
            ],
            [
                'id' =>'txtDescricao',
                'label'=>'Descrição',
                'nome'=>'descricao',
                'tamanho'=>'1000',
                'tipo'=>'textarea',
                'rows'=>'3',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'tipo'=>'array',
                'campos'=> 
                [
                    [
                        'largura'=>'11',
                        'id' =>'cbDesenvolvedor',
                        'label'=>'Desenvolvedor',
                        'nome'=>'desenvolvedor',
                        'tipo'=>'combo',
                        'opcoes'=>$desenvolvedor,
                        'required'=> true,
                        'disabled'=>false,
                        'default'=>'Selecione um Desenvolvedor'
                    ],
                    [
                        'largura'=>'1',
                        'tipo'=>'botao',
                        'id'=>'btnSalvarTarefa',
                        'icone'=>'fas fa-plus',
                        'classe'=>'btn btn-sm btn-success mt-4',
                    ],
                ]
            ],
            [
                'id'=>'tbTarefa',
                'tipo'=>'tabela',
                'cabecalho'=>['Titulo','Descricao','Desenvolvedor','Status',''],
                'classe_header'=>'bg-primary text-white'
            ]
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.permissao.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir a Permissão'
    ])

    @include('partials._modal_form',
    [
        'idModal'=>'mdlListarDesenvolvedor',
        'controles_salvar' =>'false',
        'controles_fechar'=>'true',
        'largura'=>'900',
        'icone'=>'fa-list',
        'titulo'=> 'Vincular Desenvolvedor x Projeto',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'12',
                        'id' =>'divListaDesenvolvedorProjeto',
                        'label'=>'',
                        'style'=>'height:300px;border-style:none;',
                        'tipo'=>'div'
                    ]
                ]
            ]
        ]
    ])

@endsection