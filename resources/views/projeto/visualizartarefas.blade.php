@extends('layouts.app')
@section('titulo')
<i class="fas fa-file-alt"></i> Adicionar tarefas ao pedido
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/projeto/tarefa.js') }}}"></script>
    <!-- <script src="{{{ URL::asset('js/views/projeto/cronograma.js') }}}"></script> -->
@endsection
@section('pagina') 
    <form method="POST" action="">
    @csrf
    <input id="hdnId" name="hdnId" type="hidden" value="{{$projetos->getId()}}" data="{{$projetos->getId()}}"/>
    <input id="hdnProgresso" name="hdnProgresso" type="hidden" value="{{$total}}" data="{{$total}}"/>
    <div class="container-fluid">
    <div class="card">
        <div class="card-header cor-card-header d-flex justify-content-between">
        <div><i class="fas fa-file-alt"></i> Informações do projeto </div> 
            <div class="text-right">
                <a href="{{route('projeto.cronograma',[Util::StringAleatoria(),$projetos->getId(),Util::StringAleatoria()])}}">
                    <span class="fas fa-eye btn btn-sm btn-info btn-sm cursor-pointer text-white" title="Ver cronograma"></span>
                </a></div>           
                        </div>
                        <div class="card-body">
                         <div class="table-responsive">
                         <table  id="tbProjeto" class="table table-sm table-bordered">
                            <thead>
                                <tr class="alert-secondary">
                                    <th>Sistema</th>
                                    <th class="text-center">Progresso</th>
                                    <th class="text-center">Qtd Tarefas</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Desenvolvedores</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td id="{{$projetos->getId()}}_descricao">{{$projetos->Sistema->getDescricao()}}</td>
                                        <td id="{{$projetos->getId()}}_progresso" class="text-center">{{$projetos->getProgresso()}} %</td>
                                        <td id="{{$projetos->getId()}}_quantidade" class="text-center">{{$valor}}</td>
                                        <td class="text-center">
                                            <span id="spnStatus" status="{{$projetos->getStatus()}}"
                                                class="cursor-pointer badge badge-{{STATUS_PROJETO::GetBadge($projetos->getStatus())}}">
                                                {{STATUS_PROJETO::GetTipo($projetos->getStatus())}}
                                            </span>
                                        </td>                        
                                        <td class="text-center" width="20%">
                                            <ul class="list-inline">
                                                @foreach($projetos->ProjetoUsuario as $proju)
                                                    <li>
                                                        <img src="{{empty($proju->Usuario->getFoto()) ? '../img/user.png' : '../../'.$proju->Usuario->getFoto()}}" class="avatar" height="50">
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <input type="hidden" value="{{$total}}"/>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                    </div><!--<div class="card-body">-->
</div><!-- <div class="card">-->
</br>
<div class="card">
    <div class="card-header cor-card-header d-flex justify-content-between">
            <div class="text-left">
                <i class="fas fa-file-alt"></i> Tarefas adicionadas ao projeto
            </div>
        
            <div class="text-right">
                <button id="btnAdicionarTarefa" type="button" class="btn btn-sm btn-success" aria-label="Left Align" >
                    <i class="fas fa-plus-circle"></i><b> Adicionar Tarefa</b>
                </button>
            </div>           
    </div>
    <div class="card-body">
    
        <div id="lista">
            <div class="row" >
                
                <div class="col-lg-4 col-12">
                    <div class="card">
                        <div class="container">
                            <table id="tbPendente" class="table table-striped text-left mt-2">
                                <thead>
                                    <tr>
                                        <th><i class="fas fa-check-circle"></i> A FAZER</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="addTarefaP" ondrop="dropP(event)" ondragover="allowDrop(event)">
                                    

                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                </div> 

                <div class="col-lg-4 col-12">
                    <div class="card">
                        <div class="container">
                            <table id="tbExecucao" class="table table-striped text-left mt-2">
                                <thead>
                                    <tr>
                                        <th><i class="fas fa-check-circle"></i> EM EXECUÇÃO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="addTarefaE" ondrop="dropE(event)" ondragover="allowDrop(event)">
                                    

                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                </div> 

                <div class="col-lg-4 col-12">
                    <div class="card">
                        <div class="container">
                            <table id="tbFeito" class="table table-striped text-left mt-2">
                                <thead>
                                    <tr>
                                        <th><i class="fas fa-check-circle"></i> FEITO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="addTarefaF" ondrop="dropF(event)" ondragover="allowDrop(event)">
                                    

                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
                   
    </div><!--<div class="card-body">-->
</div><!-- <div class="card">-->
        </form>
    </div><!-- <div class="container-fluid">-->
    
    <script>
        function drag(ev){
            ev.dataTransfer.setData("text", ev.target.id);
        }
        function allowDrop(ev){
            ev.preventDefault();
        }
    </script>
    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Nova Tarefa',
        'icone'=>'fa-cog',
        'largura'=>'800',
        'rota'=>'projeto.tarefa.salvar',
        'campos'=> 
        [
            [
                'id'=>'hdnProj',
                'nome'=>'hdnProj',
                'tipo'=>'hidden',
            ],
            [
                'id'=>'hdnProgress',
                'nome'=>'hdnProgress',
                'tipo'=>'hidden',
            ],
            [
                'tipo'=>'array',
                'campos'=> 
                [
                    [
                        'largura'=>'10',
                        'id' =>'txtTitulo',
                        'label'=>'Título',
                        'nome'=>'titulo',
                        'tipo'=>'txt',
                        'tamanho'=>'50',    
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'2',
                        'id' =>'txtPeso',
                        'label'=>'Peso',
                        'nome'=>'peso',
                        'tipo'=>'numero',
                        'tamanho'=>'2',
                        'min'=>'1',
                        'max'=>'10',    
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                ]
            ],
            [
                'id' =>'txtDescricao',
                'label'=>'Descrição',
                'nome'=>'descricao',
                'tamanho'=>'1000',
                'tipo'=>'textarea',
                'rows'=>'3',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'tipo'=>'array',
                'campos'=> 
                [
                    [
                        'largura'=>'12',
                        'id' =>'cbDesenvolvedor',
                        'label'=>'Desenvolvedor',
                        'nome'=>'desenvolvedor',
                        'tipo'=>'combo',
                        'opcoes'=>$desenvolvedor,
                        'required'=> true,
                        'disabled'=>false,
                        'default'=>'Selecione um Desenvolvedor'
                    ],
                ]
            ],
            [
                'tipo'=>'array',
                'campos'=> 
                [
                    [
                        'largura'=>'6',
                        'id' =>'txtDataInicio',
                        'label'=>'Data Início',
                        'nome'=>'dataInicio',
                        'tamanho'=>'20',
                        'tipo'=>'data',
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'6',
                        'id' =>'txtDataFim',
                        'label'=>'Data Fim',
                        'nome'=>'dataFim',
                        'tamanho'=>'20',
                        'tipo'=>'data',
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ]
                ]
            ]
        ]
    ])

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'idModal'=>'dlgEdicao',
        'titulo'=> 'Nova Tarefa',
        'icone'=>'fa-cog',
        'largura'=>'1000',
        'rota'=>'projeto.tarefa.salvar',
        'campos'=> 
        [
            [
                'id'=>'hdnProjE',
                'nome'=>'hdnProj',
                'tipo'=>'hidden',
            ],
            [
                'id'=>'hdnProgressE',
                'nome'=>'hdnProgress',
                'tipo'=>'hidden',
            ],
            [
                'id'=>'tabsTarefas',
                'tipo'=> 'tabs',
                'abas'=> ['Tarefa','Subtarefas'],
                'altura'=>'400px',
                'conteudo'=>
                [
                    [
                        'aba'=>'Tarefa',
                        'campos'=>
                        [
                            [
                                'tipo'=>'array',
                                'campos'=> 
                                [
                                    [
                                        'largura'=>'5',
                                        'id' =>'txtTituloE',
                                        'label'=>'Título',
                                        'nome'=>'titulo',
                                        'tipo'=>'txt',
                                        'tamanho'=>'50',    
                                        'required'=> true,
                                        'autofocus'=> true,
                                        'disabled'=>false
                                    ],
                                    [
                                        'largura'=>'5',
                                        'id' =>'cbDesenvolvedorE',
                                        'label'=>'Desenvolvedor',
                                        'nome'=>'desenvolvedor',
                                        'tipo'=>'combo',
                                        'opcoes'=>$desenvolvedor,
                                        'required'=> true,
                                        'disabled'=>false,
                                        'default'=>'Selecione um Desenvolvedor'
                                    ],  
                                    [
                                        'largura'=>'2',
                                        'id' =>'txtPesoE',
                                        'label'=>'Peso',
                                        'nome'=>'peso',
                                        'tipo'=>'numero',
                                        'tamanho'=>'2',
                                        'min'=>'1',
                                        'max'=>'10',    
                                        'required'=> true,
                                        'autofocus'=> true,
                                        'disabled'=>false
                                    ]
                                ]
                            ],
                            [
                                'id' =>'txtDescricaoE',
                                'label'=>'Descrição',
                                'nome'=>'descricao',
                                'tamanho'=>'1000',
                                'tipo'=>'textarea',
                                'rows'=>'3',
                                'required'=> true,
                                'autofocus'=> true,
                                'disabled'=>false
                            ],
                            [
                                'tipo'=>'array',
                                'campos'=> 
                                [
                                    [
                                        'largura'=>'4',
                                        'id' =>'txtDataInicioE',
                                        'label'=>'Data Início',
                                        'nome'=>'dataInicio',
                                        'tamanho'=>'20',
                                        'tipo'=>'data',
                                        'required'=> true,
                                        'autofocus'=> true,
                                        'disabled'=>false
                                    ],
                                    [
                                        'largura'=>'4',
                                        'id' =>'txtDataFimE',
                                        'label'=>'Data Fim',
                                        'nome'=>'dataFim',
                                        'tamanho'=>'20',
                                        'tipo'=>'data',
                                        'required'=> true,
                                        'autofocus'=> true,
                                        'disabled'=>false
                                    ],
                                    [
                                        'largura'=>'4',
                                        'id' =>'cbStatusE',
                                        'label'=>'Status',
                                        'nome'=>'status',
                                        'tamanho'=>'2',
                                        'tipo'=>'combo',
                                        'opcoes'=>
                                        [
                                            '0'=>'A FAZER',
                                            '1'=>'EM EXECUÇÃO',
                                            '2'=>'FEITO'
                                        ],
                                        'required'=> true,
                                        'autofocus'=> true,
                                        'disabled'=>false
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'aba'=>'Subtarefas',
                        'campos'=>
                        [
                            [
                                'id'=>'hdnSubtarefa',
                                'nome'=>'hdnSubtarefa',
                                'tipo'=>'hidden',
                            ],
                            [
                                'tipo'=>'array',
                                'campos'=> 
                                [
                                    [
                                        'largura'=>'8',
                                        'id' =>'txtTituloSub',
                                        'label'=>'Título',
                                        'nome'=>'tituloSub',
                                        'tipo'=>'txt',
                                        'tamanho'=>'50',    
                                        'required'=> false,
                                        'autofocus'=> true,
                                        'disabled'=>false
                                    ],
                                    [
                                        'largura'=>'4',
                                        'tipo'=>'botao',
                                        'id'=>'addSubTarefas',
                                        'texto'=>'Adicionar SubTarefas',
                                        'icone'=>'fas fa-plus',
                                        'classe'=>'btn btn-dark mt-4 text-left',
                                        'label'=>''
                                    ],
                                ]
                            ],
                            [
                                'id' =>'txtDescricaoSub',
                                'label'=>'Descrição',
                                'nome'=>'descricaoSub',
                                'tamanho'=>'1000',
                                'tipo'=>'textarea',
                                'rows'=>'3',
                                'required'=> false,
                                'autofocus'=> true,
                                'disabled'=>false
                            ],
                            [
                                'id'=>'tbSubtarefas',
                                'tipo'=>'tabela',
                                'cabecalho'=>['Título','Descrição','Status',''],
                                'classe_header'=>'bg-dark text-white text-center'
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'projeto.tarefa.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir a tarefa'
    ])

@endsection