@extends('layouts.app')

@section('titulo')
    <i class="fas fa-chart-line"></i> Cronograma
@endsection

@section('scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/xrange.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script src="{{{ URL::asset('js/views/projeto/cronograma.js') }}}"></script>
@endsection

@section('pagina')

<div class="container-fluid">
    <input id="hdnProjeto" type="hidden" value="{{$projeto}}"/>
    <div class="panel panel-primary" >
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary quadro-chart" style="background-color: #fff;">
                    <div class="panel-heading">
                        <div>
                            <br>
                            <div class="col-lg-12">
                                <div class="card bar-chart-example" >
                                    <div class="card-body">
                                        <div id="container"></div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection