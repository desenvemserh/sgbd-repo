@extends('layouts.app')

@section('titulo')
    <i class="fas fa-user-tag"></i> Classes
@endsection

@section('css')
    <link rel="stylesheet" href="{{{ URL::asset('js/DataTables/datatables.min.css') }}}" />
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/DataTables/datatables.min.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/adm/classes.js') }}}"></script>
@endsection

@section('pagina')

    <div class="container-fluid">
        <div class="card rounded text-left">
            <div class="card-body">
                <div class="row" >
                    <div class="col text-right">
                        <button id="btnNovaClasse" type="button" class="btn btn-primary" aria-label="Left Align" >
                            <i class="fas fa-plus-circle"></i> Nova Classe
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <div class="row" >
                    <div class="col panel panel-primary table-responsive">
                        <table id="tbClasses" class="table table-hover text-left">
                            <thead>
                                <tr>
                                    <th>Sistema</th>
                                    <th>Código</th>
                                    <th>Descrição</th>
                                    <th>Admin</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($classes as $cls)
                                    <tr>
                                        <td id="{{$cls->getId()}}_sistema" data="{{$cls->Sistema->getId()}}">{{$cls->Sistema->getDescricao()}}</td>
                                        <td id="{{$cls->getId()}}_codigo">{{$cls->getCodigo()}}</td>
                                        <td id="{{$cls->getId()}}_descricao">{{$cls->getDescricao()}}</td>
                                        <td id="{{$cls->getId()}}_admin" data="{{$cls->isAdmin()}}">

                                            @if($cls->isAdmin())
                                                <span class="badge badge-success">Sim</span>
                                            @else
                                                <span class="badge badge-danger">Não</span>
                                            @endif


                                        </td>
                                        <td width="10%">
                                            <span name="editBtn" class="fas fa-edit" data="{{$cls->getId()}}" style="cursor: pointer;color: #009933;" title="Editar"></span>
                                            <span name="delBtn" class="fas fa-trash-alt" data="{{$cls->getId()}}" style="cursor: pointer;color: #ff0000;" title="Excluir"></span>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Nova Classe',
		'icone'=>'fa-user-tag',
        'rota' => 'adm.classe.salvar',
        'campos'=>
        [
            [
                'largura'=>'4',
                'id' =>'cbAdmin',
                'label'=>'Administrador?',
                'nome'=>'admin',
                'tipo'=>'slider',
                'required'=> true
            ],
            [
                'id' =>'cbSistema',
                'label'=>'Sistema',
                'nome'=>'sistema',
                'tipo'=>'combo',
                'opcoes'=>$sistemas,
                'default'=>'Selecione um Sistema',
                'required'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'txtCodigo',
                'label'=>'Código',
                'nome'=>'codigo',
                'tamanho'=>'5',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'txtDescricao',
                'label'=>'Descrição',
                'nome'=>'descricao',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ]
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.classe.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir a Classe'
    ])

@endsection
