@extends('layouts.app')

@section('titulo')
    <i class="fas fa-laptop"></i> Sistemas
@endsection

@section('css')
    <link rel="stylesheet" href="{{{ URL::asset('js/DataTables/datatables.min.css') }}}" />
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/DataTables/datatables.min.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/adm/sistemas.js') }}}"></script>
@endsection

@section('pagina')
    
    <div class="container-fluid">
        <div class="card rounded text-left">
            <div class="card-body">
                <div class="row" >
                    <div class="col text-right">
                        <button id="btnNovoSistema" type="button" class="btn btn-primary" aria-label="Left Align" >
                            <i class="fas fa-plus-circle"></i> Novo Sistema
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-body">
                <div class="row" >
                    <div class="col panel panel-primary table-responsive">
                        <table id="tbSistema" class="table table-hover text-left">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Descrição</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($sistemas as $sist)
                                        <tr>
                                            <td id="{{$sist->getId()}}_codigo">{{$sist->getCodigo()}}</td>
                                            <td id="{{$sist->getId()}}_descricao">{{$sist->getDescricao()}}</td>
                                            <td width="10%">
                                                <span name="editBtn" class="fas fa-edit" data="{{$sist->getId()}}" style="cursor: pointer;color: #009933;" title="Editar"></span>
                                                <span name="delBtn" class="fas fa-trash-alt" data="{{$sist->getId()}}" style="cursor: pointer;color: #ff0000;" title="Excluir"></span>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
		'icone'=>'fa-laptop',
        'titulo'=> 'Novo Sistema',
        'rota' => 'adm.sistema.salvar',
        'campos'=> 
        [
            [
                'id' =>'txtCodigo',
                'label'=>'Código',
                'nome'=>'codigo',
                'tamanho'=>'5',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'txtDescricao',
                'label'=>'Descrição',
                'nome'=>'descricao',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ]
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.sistema.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir o Sistema'
    ])

@endsection