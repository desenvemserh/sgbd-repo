@extends('layouts.app')

@section('titulo')
    <i class="fas fa-user"></i> Cadastro de Funcionários
@endsection

@section('scripts')
    
@endsection

@section('pagina')


    <div class="container-fluid">
        <div class="card rounded text-left">
            <div class="card-body">
                <form method="GET" action="{{route('adm.funcionario.pesquisar')}}">
                    <div class="row" >
                        <div class="col col-sm-3 text-left">
                            <label for="txtNomePesquisa" class="col-form-label text-md-left">Nome</label>
                            <br/>
                            <input id="txtNomePesquisa" name="nome" class="form-control" type="text"/>
                        </div>

                        <div class="col col-sm-2 text-left">
                            <label for="txtCpfPesquisa" class="col-form-label text-md-left">CPF</label>
                            <br/>
                            <input id="txtCpfPesquisa" name="cpf" class="form-control mask_cpf" type="text"/>
                        </div>

                        <div class="col col-sm-2 text-left">
                            <label for="txtIdentidadePesquisa" class="col-form-label text-md-left">Identidade</label>
                            <br/>
                            <input id="txtIdentidadePesquisa" name="identidade" class="form-control" type="text"/>
                        </div>

                        <div class="col col-sm-2 text-md-left">
                            <label class="col-form-label text-md-left">&nbsp;</label>
                            <br/>
                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i> Pesquisar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <div class="row" >
                    <div class="col panel panel-primary table-responsive">
                        <table id="tbFuncionario" class="table table-hover text-left">
                            <thead>
                                <tr>
                                    <th>Identificação Alterdata</th>
                                    <th>Nome</th>
                                    <th>CPF</th>
                                    <th>Identidade</th>
                                    <th>Data Nascimento</th>
                                    <th>Data Admissão</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($funcionarios as $func)
                                    <tr>
                                        <td id="{{$func->getId()}}_alterdata">{{$func->getId()}}</td>
                                        <td id="{{$func->getId()}}_nome">{{$func->getNome()}}</td>
                                        <td id="{{$func->getId()}}_cpf">{{$func->getCpf()}}</td>
                                        <td id="{{$func->getId()}}_identidade">{{$func->getIdentidade()}}</td>
                                        <td id="{{$func->getId()}}_dt_nascimento">{{$func->getDtNascimento()}}</td>   
                                        <td id="{{$func->getId()}}_dt_admissao">{{$func->getDtAdmissao()}}</td>
                                    </tr>
                                @endforeach
                                @if(count($funcionarios) == 0)
                                    <tr>
                                        <td colspan="6" align="center">Nenhum Funcionário encontrado</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="card-footer">
                            {{$funcionarios->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection