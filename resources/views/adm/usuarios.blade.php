@extends('layouts.app')

@section('titulo')
    <i class="fas fa-user"></i> Cadastro de Usuários
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/adm/usuario.js') }}}"></script>
@endsection

@section('pagina')


    <div class="container-fluid">
        <div class="card rounded text-left">
            <div class="card-body">
                <form method="GET" action="{{route('adm.usuario.pesquisar')}}">
                    <div class="row" >
                        <div class="col col-sm-3 text-left">
                            <label for="txtNomePesquisa" class="col-form-label text-md-left">Nome</label>
                            <br/>
                            <input id="txtNomePesquisa" name="nome" class="form-control" type="text"/>
                        </div>

                        <div class="col col-sm-2 text-left">
                            <label for="txtCpfPesquisa" class="col-form-label text-md-left">CPF</label>
                            <br/>
                            <input id="txtCpfPesquisa" name="cpf" class="form-control mask_cpf" type="text"/>
                        </div>

                        <div class="col col-sm-2 text-md-left">
                            <label class="col-form-label text-md-left">&nbsp;</label>
                            <br/>
                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i> Pesquisar</button>
                        </div>

                        <div class="col text-right">
                            <button id="btnNovoUsuario" type="button" class="btn btn-primary" aria-label="Left Align" >
                                <i class="fas fa-plus-circle"></i> Novo Usuário
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <div class="row" >
                    <div class="col panel panel-primary table-responsive">
                        <table id="tbUsuarios" class="table table-hover text-left">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>CPF</th>
                                    <th>E-mail</th>
                                    <th>Funcionário</th>
                                    <th>Unidade</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($usuarios as $usu)
                                    <tr>
                                        <td id="{{$usu->getId()}}_nome">{{$usu->getNome()}}</td>
                                        <td id="{{$usu->getId()}}_cpf">{{$usu->getCpf()}}</td>
                                        <td id="{{$usu->getId()}}_email">{{$usu->getEmail()}}</td>
                                        @if($usu->getFkFuncionario() != '')
                                            <td id="{{$usu->getId()}}_funcionario" data="{{$usu->Funcionario->getId()}}">{{$usu->Funcionario->getNome()}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($usu->getFkFuncionario() != '')
                                            <td id="{{$usu->getId()}}_unidade">{{$usu->Funcionario->Unidade->getNome()}}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td id="{{$usu->getId()}}_ativo" data="{{$usu->getAtivo()}}">
                                            @if($usu->getAtivo())
                                                <span class="badge badge-success">Ativo</span>
                                            @else
                                                <span class="badge badge-danger">Inativo</span>
                                            @endif
                                        </td>
                                        <td>
                                            <span name="editBtn" class="cursor-pointer fas fa-edit" data="{{$usu->getId()}}" style="color: #009933;" title="Editar"></span>
                                            <span name="delBtn" class="cursor-pointer fas fa-trash-alt" data="{{$usu->getId()}}" style="color: #ff0000;" title="Excluir"></span>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div style="text-align" class="card-footer">
                            {{$usuarios->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Novo Usuário',
        'rota' => 'adm.usuario.salvar',
        'largura'=>'700',
        'icone' => 'fa-user',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'8',
                        'padding'=>'50',
                        'id' =>'txtCpf',
                        'label'=>'CPF',
                        'nome'=>'cpf',
                        'classe'=>'mask_cpf',
                        'tamanho'=>'11',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true
                    ],
                    [
                        'largura'=>'4',
                        'id' =>'cbAtivo',
                        'label'=>'Ativo',
                        'nome'=>'ativo',
                        'tipo'=>'slider',
                        'required'=> true
                    ],
                ]
            ],
            [
                'id' =>'txtNome',
                'label'=>'Nome',
                'nome'=>'nome',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'cbFuncionario',
                'label'=>'Funcionário',
                'nome'=>'Funcionário',
                'tipo'=>'combo',
                'opcoes'=>[],
                'required'=> false,
                'disabled'=>false
            ],
            [
                'id' =>'txtEmail',
                'label'=>'E-mail',
                'nome'=>'email',
                'tamanho'=>'100',
                'tipo'=>'email',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'6',
                        'id' =>'txtSenha',
                        'label'=>'Senha',
                        'nome'=>'senha',
                        'tamanho'=>'50',
                        'tipo'=>'password',
                        'required'=> true,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'6',
                        'id' =>'txtConfirmSenha',
                        'label'=>'Confirmar senha',
                        'nome'=>'confirma_senha',
                        'tamanho'=>'50',
                        'tipo'=>'password',
                        'required'=> true,
                        'disabled'=>false
                    ]
                ]
            ],
            /*[
                'id' =>'txtFoto',
                'label'=>'Foto',
                'nome'=>'foto',
                'tamanho'=>'50',
                'tipo'=>'arquivo',
                'required'=> false,
                'autofocus'=> true,
                'disabled'=>false
            ],*/
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.usuario.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir o Usuário'
    ])

@endsection
