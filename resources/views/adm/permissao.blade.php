@extends('layouts.app')

@section('titulo')
    <i class="fas fa-key"></i> Permissões
@endsection

@section('css')
    <link rel="stylesheet" href="{{{ URL::asset('js/DataTables/datatables.min.css') }}}" />
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/DataTables/datatables.min.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/adm/permissao.js') }}}"></script>
@endsection

@section('pagina')
    
    <div class="container-fluid">
        <div class="card rounded text-left">
            <div class="card-body">
                <div class="row" >
                    <div class="col text-right">
                        <button id="btnNovoParametro" type="button" class="btn btn-primary" aria-label="Left Align" >
                            <i class="fas fa-plus-circle"></i> Nova Permissão
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <div class="row" >
                    <div class="col panel panel-primary table-responsive">
                        <table id="tbPermissao" class="table table-hover text-left">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Usuário</th>
                                    <th>Classe</th>
                                    <th>Sistema</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($permissoes as $permissao)
                                    <tr>
                                        <td id="{{$permissao->getId()}}_codigo">{{$permissao->getId()}}</td>
                                        <td id="{{$permissao->getId()}}_fk_usuario" data="{{$permissao->getFkUsuario()}}">{{$permissao->Usuario->getNome()}}</td>
                                        <td id="{{$permissao->getId()}}_fk_classe" data="{{$permissao->getFkClasse()}}">{{$permissao->Classe->getDescricao()}}</td>
                                        <td id="{{$permissao->getId()}}_sistema" data="{{$permissao->Classe->Sistema->getId()}}">{{$permissao->Classe->Sistema->getDescricao()}}</td>
                                        <td width="10%">
                                            <span name="editBtn" class="fas fa-edit" data="{{$permissao->getId()}}" style="cursor: pointer; color: #009933;" title="Editar"></span>
                                            <span name="delBtn" class="fas fa-trash-alt" data="{{$permissao->getId()}}" style="cursor: pointer;color: #ff0000;" title="Excluir"></span>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Nova Permissão',
		'icone'=>'fa-cog',
        'rota'=>'adm.permissao.salvar',
        'campos'=> 
        [
            [
                'id' =>'cbUsuario',
                'label'=>'Usuário',
                'nome'=>'usuario',
                'tipo'=>'combo',
                'opcoes'=>$usuarios,
                'required'=> true,
                'disabled'=>false,
                'default'=>'Selecione um Usuário'
            ],
            [
                'id' =>'cbSistema',
                'label'=>'Sistema',
                'nome'=>'sistema',
                'tipo'=>'combo',
                'opcoes'=>$sistemas,
                'required'=> true,
                'disabled'=>false,
                'default'=>'Selecione um Sistema'
            ],
            [
                'id' =>'cbClasse',
                'label'=>'Classes',
                'nome'=>'classe',
                'tipo'=>'combo',
                'opcoes'=>[],
                'required'=> true,
                'disabled'=>false
            ],
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.permissao.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir a Permissão'
    ])

@endsection