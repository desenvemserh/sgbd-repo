<style>

/*  bhoechie tab */
div.bhoechie-tab-container
{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid transparent;
  margin-left: 10px;
  /*
  margin-top: 20px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
*/
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu
{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group
{
  margin-bottom: 0;
}

div.bhoechie-tab-menu div.list-group>a
{
  margin-bottom: 0;
}

div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa
{
  color: #5A55A3;
}

div.bhoechie-tab-menu div.list-group>a:first-child
{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}

div.bhoechie-tab-menu div.list-group>a:last-child
{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa
{
  background-color: #5d811f;
  background-image: #5d811f;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after
{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #5d811f;
}

div.bhoechie-tab-content
{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;

  max-height: 590px;
  overflow-y: auto;
  overflow-x: hidden;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active)
{
  display: none;
}
    </style>

<div id="{{(empty($idModal) ? 'dlgTabs' : $idModal)}}" class="modal fade {{(!empty($classeModal) ? $classeModal : '')}}"  tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg " {{empty($responsivo) ? ('style=width:'.(empty($largura) ? '2000' : $largura).'px') : '' }} >
        <!-- Modal content-->
        <div class="modal-content" role="document">
            <div class="modal-header {{(!empty($classeHeader) ? $classeHeader : '')}}">
                <h4 class="modal-title"><i class="fas {{(empty($icone) ? '' : $icone)}}"></i> <txt id="titulo_form_modal{{(empty($idModal) ? '' :'_'.$idModal)}}">{{ $titulo }}</txt></h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">

                <input id="hdnIdChamadoModal" type="hidden"/>

                    <div class="bhoechie-tab-container">
                        <div class="row">

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                            <div class="list-group">
                                <a id="menuChamado" href="#" class="list-group-item active text-center">
                                    <h4 class="fas fa-bullhorn"></h4>
                                    <br/>Detalhes
                                </a>
                                <a id="menuAnexo" href="#" class="list-group-item text-center">
                                    <h4 class="fas fa-paperclip"></h4>
                                    <br/>Anexos
                                </a>
                                <a id="menuHistorico" href="#" class="list-group-item text-center">
                                    <h4 class="fas fa-history"></h4>
                                    <br/>Histórico
                                </a>
                                <br/>
                                <button id="btnAddComment" class="form-control btn btn-info"><i class="fas fa-comment"></i> Adicionar Comentário</button>
                                <br/>
                                <button id="btnMudarPrioridade" class="form-control btn btn-info"><i class="fas fa-exclamation text-danger"></i> Alterar Prioridade</button>
                                <br/>
                                <button id="btnMudarStatus" class="form-control btn btn-info"><i class="fas fa-exchange-alt text-dark"></i> Alterar Status</button>
                                <br/>
                                <button id="btnDelegar" class="form-control btn btn-info"><i class="fas fa-arrow-right"></i><i class="fas fa-user-tie"></i> Delegar Chamado</button>

                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab" style="min-height: 590px;">


                            <!-- detalhes do chamado -->
                            <div class="bhoechie-tab-content active">

                                <form>

                                    <div class="form-group row">
                                        <div class="col col-form col-sm-3 ">
                                            <label id="txtDtCriacao_label" for="txtDtCriacao" class="text-md-left">Data abertura</label>
                                            <input id="txtDtCriacao" class="form-control " type="text" maxlength="10" name="dt_abertura" disabled="">
                                        </div>


                                        <div class="col col-form col-sm-4 text-center">
                                            <label id="spnPrioridade_label" for="spnPrioridade" class="text-md-left">Prioridade</label>

                                            <h3><span id="spnPrioridade" name="" class="badge badge-primary font-weight-bold">em Análise</span></h3>
                                        </div>


                                        <div class="col col-form col-sm-4 text-center">
                                            <label id="spnStatus_label" for="spnStatus" class="text-md-left">Status</label>

                                            <h3> <span id="spnStatus" name="" class="badge badge-primary font-weight-bold"></span></h3>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col col-form col-sm-12">
                                            <label id="txtUnidade_label" for="txtUnidade" class="col-form-label text-md-left">Unidade</label>
                                            <input id="txtUnidade" class="form-control " type="text" maxlength="50" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col col-form col-sm-6">
                                            <label id="txtSetor_label" for="txtSetor" class="col-form-label text-md-left">Setor</label>
                                            <input id="txtSetor" class="form-control" type="text" maxlength="50" disabled>
                                        </div>

                                        <div class="col col-form col-sm-6">
                                            <label id="txtDono_label" for="txtDono" style="padding-right:20px;" class="col-form-label text-md-left">Aberto por</label>
                                            <input id="txtDono" class="form-control " type="text" maxlength="50" name="dono" disabled="">
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <div class="col col-form col-sm-12 ">
                                            <label id="txtEquipamento_label" for="txtEquipamento" style="padding-right:20px;" class="col-form-label text-md-left">Equipamento</label>
                                            <input id="txtEquipamento" class="form-control " type="text" maxlength="50" name="equipe" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col col-form col-sm-12 ">
                                            <label id="txtResponsavel_label" for="txtResponsavel" style="padding-right:20px;" class="col-form-label text-md-left">Analista Responsável</label>
                                            <input id="txtResponsavel" class="form-control " type="text" maxlength="50" name="analista" disabled="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col col-form col-sm-12 ">
                                            <label id="txtDescricao_label" for="txtDescricao" style="padding-right:20px;" class="col-form-label text-md-left">Descricao</label>
                                            <textarea id="txtDescricao" class="form-control noresize" type="textarea" maxlength="200" name="equipe" rows="2" disabled=""></textarea>
                                        </div>
                                    </div>

                                </form>

                            </div>

                            <!-- anexos do chamado -->
                            <div id="conteudoAnexo" class="bhoechie-tab-content">

                                <div class="row">
                                </div>


                            </div>

                            <!-- histórico -->
                            <div id="conteudoHistorico" class="bhoechie-tab-content">





                                <!-- Comentário Usuario-->
                                <div class="container-fluid">
                                <div class="media text-muted pt-3">
                                    <i class="fas fa-user" style="font-size: 30px;"></i>&nbsp;&nbsp;
                                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                        <strong class="d-block text-dark">Florinda Girafalles - 10/12/2018 15:50:40</strong>
                                        Só mentira. Nunca falou comigo.
                                    </p>
                                    </div>
                                </div>


                                <!-- Comentário Analista -->
                                <div class="container-fluid">
                                    <div class="media text-muted pt-3">
                                        <i class="fas fa-user-tie" style="font-size: 30px;"></i>&nbsp;&nbsp;
                                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                            <strong class="d-block text-dark">Sidney Figueira - 10/12/2018 15:10:50</strong>
                                            Conforme contato com o solcitante, foi agendado uma data para visita técnica.
                                        </p>
                                    </div>
                                </div>

                                <!-- Atualização de status Analista-->
                                <div class="container-fluid">
                                    <div class="media text-muted pt-3">
                                        <i class="fas fa-user-tie" style="font-size: 30px;"></i>&nbsp;&nbsp;
                                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                            <strong class="d-block text-dark">Sidney Figueira - 10/12/2018 14:35:23</strong>
                                            Atualizaçao de Status. De: <span class="badge badge-pills badge-warning font-weigth-bold">Pendente</span> para <span class="badge badge-pills badge-primary font-weigth-bold">em Análise.</span>
                                        </p>
                                    </div>
                                </div>





                            </div>


                        </div>
                    </div>

                </div>


            </div>

        </div>
    </div>
</div>
