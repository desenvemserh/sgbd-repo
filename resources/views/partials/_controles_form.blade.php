

@if($controle["tipo"] == 'hidden')

    <input type="hidden" id="{{$controle['id']}}" name="{{$controle['nome']}}" />

@elseif($controle["tipo"] == 'slider')

    <div class="form-group row mb-0">
        <div class="col col-form">
            <label class="switch  pull-right">
                <input id="{{$controle['id']}}" name="{{$controle['nome']}}" type="checkbox" checked>
                <span class="slider round"></span>
            </label>
        </div>
    </div>

@elseif($controle["tipo"] == 'txt')

    <input id="{{$controle['id']}}"
        class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
        type="text"
        maxlength="{{$controle['tamanho']}}"
        name="{{$controle['nome']}}"
        {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
        {{ (!empty($controle["autocomplete"])  ? 'autocomplete='.$controle["autocomplete"] : '') }}
        {{ (!empty($controle["placeholder"]) ? 'placeholder='.$controle["placeholder"] :'') }}
        {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
        {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
        {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }} />

@elseif($controle["tipo"] == 'password')

    <input id="{{$controle['id']}}"
        class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
        type="password"
        maxlength="{{$controle['tamanho']}}"
        name="{{$controle['nome']}}"
        {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
        {{ (!empty($controle["autocomplete"])  ? 'autocomplete='.$controle["autocomplete"] : '') }}
        {{ (!empty($controle["placeholder"]) ? 'placeholder='.$controle["placeholder"] :'') }}
        {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
        {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
        {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }} />

@elseif($controle["tipo"] == 'email')

    <input id="{{$controle['id']}}"
        class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
        type="email"
        maxlength="{{$controle['tamanho']}}"
        name="{{$controle['nome']}}"
        {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
        {{ (!empty($controle["autocomplete"])  ? 'autocomplete='.$controle["autocomplete"] : '') }}
        {{ (!empty($controle["placeholder"]) ? 'placeholder='.$controle["placeholder"] :'') }}
        {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
        {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
        {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }} />

@elseif($controle["tipo"] == 'div')

    <div id="{{$controle['id']}}"
        class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
        {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
        {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }}>
    </div>

@elseif($controle["tipo"] == 'textarea')

    <textarea id="{{$controle['id']}}"
            class="form-control noresize"
            type="textarea"
            maxlength="{{$controle['tamanho']}}"
            name="{{$controle['nome']}}"
            rows="{{$controle['rows']}}"
            {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
            {{ (!empty($controle["autocomplete"])  ? 'autocomplete='.$controle["autocomplete"] : '') }}
            {{ (!empty($controle["placeholder"]) ? 'placeholder='.$controle["placeholder"] :'') }}
            {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
            {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
            {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }}></textarea>

@elseif($controle["tipo"] == 'arquivo')

    <input id="{{$controle['id']}}"
            type="file"
            class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
            name="{{$controle['nome']}}"
            {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
            {{ (!empty($controle["filtro"]) ?  'accept='.$controle["filtro"] : '') }}
            {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
            {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
            {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }}/>

@elseif($controle["tipo"] == 'icone')

    <i id="{{$controle['id']}}"
        class="{{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
        {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }} ></i>

@elseif($controle["tipo"] == 'combo')

    <select id="{{$controle['id']}}"
            class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
            name="{{$controle['nome']}}"
            {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
            {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
            {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
            {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }}>

        @if(!empty($controle['default']))
            <option value="">{{$controle['default']}}</option>
        @endif

        @foreach($controle['opcoes'] as $valor=>$nome)

            <option value="{{$valor}}">{{$nome}}</option>

        @endforeach

    </select>


@elseif($controle["tipo"] == 'span')

<br/>

    @if(!empty($controle['tag']))
        <{{$controle['tag']}}>
    @endif

    <span id="{{$controle['id']}}"
          name="{{(!empty($controle['nome']) ? $controle['nome'] : '' )}}"
          class="form-control {{(!empty($controle['classe']) ? $controle['classe'] : '' )}}"
          data="{{(!empty($controle['dado']) ? $controle['dado'] : '' )}}"
          title="{{(!empty($controle['title']) ? $controle['title'] : '' )}}">

          {{(!empty($controle['valor']) ? $controle['valor'] : '' )}}

    </span>

    @if(!empty($controle['tag']))
        </{{$controle['tag']}}>
    @endif


@elseif($controle["tipo"] == 'botao')
    <button id="{{$controle['id']}}"
            type="button"
            class="form-control {{(!empty($controle['classe']) ? $controle['classe'] : '' )}}"
            data="{{(!empty($controle['dado']) ? $controle['dado'] : '' )}}"
            title="{{(!empty($controle['title']) ? $controle['title'] : '' )}}">

            <i class="{{(!empty($controle['icone']) ? $controle['icone'] : '' )}}"></i>

            {{(!empty($controle['texto']) ? $controle['texto'] : '' )}}

    </button>

    @elseif($controle["tipo"] == 'tabela')
<table id="{{$controle['id']}}"
        {{(!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
       class="table table-hover table-striped {{(!empty($controle['classe']) ? $controle['classe'] : '' )}}">
    <thead class="{{(!empty($controle['classe_header']) ? $controle['classe_header'] : '' )}}">
        <tr>
            @foreach($controle['cabecalho'] as $cabecalho)
                <th>{{$cabecalho}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

@elseif($controle["tipo"] == 'numero')

<input id="{{$controle['id']}}"
    class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
    type="number"
    maxlength="{{$controle['tamanho']}}"
    min="{{$controle['min']}}"
    max="{{$controle['max']}}"
    name="{{$controle['nome']}}"
    {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
    {{ (!empty($controle["autocomplete"])  ? 'autocomplete='.$controle["autocomplete"] : '') }}
    {{ (!empty($controle["placeholder"]) ? 'placeholder='.$controle["placeholder"] :'') }}
    {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
    {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
    {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }} 
    oninput="validity.valid ? this.save = value : value = this.save;"/>

@elseif($controle["tipo"] == 'label')

    <label id="{{$controle['id']}}" class="mt-4">{{$controle["descricao"]}}</label>

@elseif($controle["tipo"] == 'data')

    <input id="{{$controle['id']}}"
        class="form-control {{ (!empty($controle["classe"]) ? $controle["classe"] : '' ) }}"
        type="date"
        maxlength="{{$controle['tamanho']}}"
        name="{{$controle['nome']}}"
        {{ (!empty($controle["style"]) ? 'style='.$controle["style"] : '' ) }}
        {{ (!empty($controle["autocomplete"])  ? 'autocomplete='.$controle["autocomplete"] : '') }}
        {{ (!empty($controle["placeholder"]) ? 'placeholder='.$controle["placeholder"] :'') }}
        {{ (!empty($controle["required"]) && $controle["required"] == 'true' ? 'required':'') }}
        {{ (!empty($controle["autofocus"]) && $controle["autofocus"] == 'true' ? 'autofocus' :'') }}
        {{ (!empty($controle["disabled"]) && $controle['disabled'] == 'true' ? 'disabled' : '') }} />

@endif
