<nav id="sidebar">
        <div class="sidebar-header">
			<h3>
				<center>
				    <img src="{{{ URL::asset('img/logo.png')}}}" class="rounded" height="80" style="margin:-10px;"/>
				</center>
			</h3>
            
        </div>

        <ul id="lstMenu" class="list-unstyled components">

            <li>
                <a href="{{route('home')}}">
                    <i class="fas fa-home fa-lg"></i>
                    Início
                </a>
            </li>


            @foreach(Auth::user()->Classe->first()->RotasMenu as $rota)

                    @if($rota->Rotas->count() > 0)

                        <li>
                            <a href="#Submenu_{{$rota->getId()}}" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                <i class="{{$rota->getIcone()}}"></i> {{$rota->getNome()}}
                            </a>
                            <ul class="collapse list-unstyled" id="Submenu_{{$rota->getId()}}">

                                @foreach($rota->Rotas as $subRota)

                                    @if($subRota->isMenu())
                                        <li>
                                            <a href="{{route($subRota->getRota())}}"><i class="{{$subRota->getIcone()}}"></i>{{$subRota->getNome()}}</a>
                                        </li>
                                    @endif

                                @endforeach

                            </ul>
                        </li>

                    @elseif(empty($rota->RotaPai))

                        <li>
                            <a href="{{route($rota->getRota())}}">
                                <i class="{{$rota->getIcone()}}"></i>
                                {{$rota->getNome()}}
                            </a>
                        </li>

                    @endif
            @endforeach


    </nav>
