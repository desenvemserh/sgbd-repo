<div class="form-group row">

    <!-- mais de um campo na mesma linha -->
    @if($controle["tipo"] == 'array')

        @for($c = 0; $c < count($controle['campos']); $c++)

            <div class="col col-form col-sm-{{$controle['campos'][$c]['largura']}} {{(!empty($controle['campos'][$c]['classeColuna']) ? $controle['campos'][$c]['classeColuna'] : '' )}}
                      {{!empty($sem_padding) && $sem_padding == 'true' ? 'p-0' : '' }} {{($c > 0 ? 'pl-0' : '')}}"
                 style="{{($c == (count($controle['campos']) - 1) ? '' : 'padding-right:'.(empty($controle['campos'][$c]['padding']) ? '10' : $controle['campos'][$c]['padding']).'px;')}}">

                @if(!empty($controle['campos'][$c]['label']))
                    <label id="{{$controle['campos'][$c]['id']}}_label" for="{{$controle['campos'][$c]['id']}}" class="text-md-left">{{$controle['campos'][$c]['label']}}</label>
                @endif

                @include('partials._controles_form',['controle'=>$controle['campos'][$c]])
            </div>

        @endfor

    @else

    <div class="col col-form col-sm-{{(!empty($controle['campos']['largura']) ? $controle['campos']['largura'] : '12')}}
                {{(!empty($controle['campos']['classe']) ? $controle['campos']['classe'] : '' )}}
                {{!empty($sem_padding) && $sem_padding == 'true' ? 'p-0' : '' }}"
         style="{{(!empty($controle['campos']['largura']) ? 'padding-right:10px' : '')}}">


         @if(!empty($controle['label']))
            <label id="{{$controle['id']}}_label" for="{{$controle['id']}}" style="padding-right:20px;" class="col-form-label text-md-left">{{$controle['label']}}</label>
        @endif

        @include('partials._controles_form',['controle'=>$controle])
    </div>
    @endif

</div>
