@extends('layouts.app')

@section('titulo')
    <i class="fas fa-chart-line"></i> Dashboard
@endsection

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-picklist/picklist.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('js/nprogress/nprogress.css') }}" >
@endsection

@section('scripts')
    <!-- <script src="{{{ URL::asset('js/charts/highcharts.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts//highcharts-more.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts/modules/solid-gauge.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts/modules/exporting.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts/modules/export-data.js') }}}"></script>

    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script> -->
    <script src="{{{ URL::asset('js/jquery-picklist/picklist.js') }}}"></script>
    <script src="{{{ URL::asset('js/nprogress/nprogress.js') }}}"></script>
    <script src="{{{ URL::asset('js/bootstrap-progressbar/bootstrap-progressbar.min.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/home.js') }}}"></script>
@endsection

@section('pagina')

    <div class="container-fluid">
		<div class="panel panel-primary ">

            <div class="row">

                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #363636;">
                        <a href="{{route('adm.usuario')}}">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <i class="fas fa-users fa-4x text-white mt-3"></i>
                                    <h0 class="float-right text-white">{{$usuarios}}</h0>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h5 class="text-white font-weight-bold">Total Usuários</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #4F4F4F;">
                        <a href="{{route('adm.funcionario')}}">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <i class="fa fa-users-cog fa-4x text-white mt-3"></i>
                                    <h0 class="float-right text-white">{{$funcionarios}}</h0>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h5 class="text-white font-weight-bold">Total Funcionários</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #696969;">
                        <a href="{{route('adm.sistema')}}">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <i class="fas fa-laptop fa-4x text-white mt-3"></i>
                                    <h0 class="float-right text-white">{{$sistemas}}</h0>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h5 class="text-white font-weight-bold">Total Sistemas</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #808080;">
                        <a href="{{route('adm.permissao')}}">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <i class="fas fa-key fa-4x text-white mt-3"></i>
                                    <h0 class="float-right text-white">{{$permissoes}}</h0>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <h5 class="text-white font-weight-bold">Tempo Permissões</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>

            <br/>
            <!-- <div class="row">

                <div class="col-md-8" >
                    <div class="panel panel-primary quadro-chart" style="background-color: #fff;">
                        <div class="panel-heading">
                            <div class="container">
                                <br>
                                <div class="col-lg-12">
                                    <h5 style="color: #999999;">Chamados em 2018</h5>
                                    <div class="card bar-chart-example">
                                        <div class="card-body">
                                            <div id="chartContainer" style="height: 400px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4" >
                    <div class="panel panel-primary quadro-chart" style="background-color: #fff;">
                        <div class="panel-heading">
                            <div class="container">
                                <br>
                                <div class="col-lg-12">
                                    <h5 style="color: #999999;">Posição dos Chamados</h5>
                                    <div class="card bar-chart-example">
                                        <div class="card-body">
                                            <div id="chartContainer2" style="height: 400px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

            </div> -->

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Projetos</h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>Simple table with project listing with progress and editing options</p> -->

                    <!-- start project list -->
                    <table id="tbProjetos" class="table table-striped projects">
                      <thead>
                        <tr>
                          <!-- <th style="width: 1%">#</th> -->
                          <th style="width: 20%">Projetos</th>
                          <th>Membros</th>
                          <th class="text-center">Progresso do Projeto</th>
                          <th>Status</th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($projetos as $projeto)
                        <tr>
                          <!-- <td>#</td> -->
                          <td>
                            <a>{{$projeto->Sistema->getDescricao()}}</a>
                            <br />
                            <!-- <small>Created 01.01.2015</small> -->
                          </td>
                          <td width="25%">
                            <ul class="list-inline">
                                @foreach($projeto->ProjetoUsuario as $proju)
                                    <li>
                                        <img src="{{empty($proju->Usuario->getFoto()) ? '../img/user.png' : '../'.$proju->Usuario->getFoto()}}" class="avatar">
                                    </li>
                                @endforeach
                            </ul>
                          </td>
                          <td>
                            <!-- <div class="progress progress_sm">
                              <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{$projeto->getProgresso()}}"></div>
                            </div>
                            <small>{{$projeto->getProgresso()}} %</small> -->
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped bg-secondary" role="progressbar" style="width: {{$projeto->getProgresso()}}%" aria-valuenow="{{$projeto->getProgresso()}}" aria-valuemin="0" aria-valuemax="100">{{$projeto->getProgresso()}}%</div>
                            </div>
                          </td>
                          <td>
                              @if($projeto->getStatus() == 1)
                                <span class="badge badge-success btn-xs">Success</span>
                              @else
                                <span class="badge badge-info btn-xs">Aberto</span>
                              @endif
                          </td>
                          <td class="text-center">
                            <a href="{{route('projeto.projeto')}}" class="btn btn-sm btn-primary"><i class="fa fa-folder"></i> Projeto </a>
                            <!-- <a href="#" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> Delete </a> -->
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>
          </div>

	    </div>

    </div>

    @include('partials._modal_form',
    [
        'idModal'=>'mdlListarDesenvolvedor',
        'controles_salvar' =>'false',
        'controles_fechar'=>'true',
        'largura'=>'900',
        'icone'=>'fa-list',
        'titulo'=> 'Vincular Desenvolvedor x Sistema',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'12',
                        'id' =>'divListaDesenvolvedorSistema',
                        'label'=>'',
                        'style'=>'height:300px;border-style:none;',
                        'tipo'=>'div'
                    ]
                ]
            ]
        ]
    ])


@endsection
