{{$linhas}}

@foreach($linhas as $l)

    <h1>{{$l->teste_nome}} idade: {{$l->teste_idade}}</h1>

@endforeach


<style>

    .tabela_teste
    {
        border: solid 1px red;
    }


</style>

<table class="tabela_teste">
    <thead>
        <th class="tabela_teste">Nome</th>
        <th class="tabela_teste">Idade</th>
    </thead>
    <tbody>

        @foreach($linhas as $teste2)
            <tr>
                <td>{{$teste2->teste_nome}}</td>
                <td>{{ $teste2->teste_idade > 30 ? ('Velho:'.$teste2->teste_idade) : 'novo'  }}</td>
            </tr>
        @endforeach

    </tbody>
</table>
