@extends('layouts.app')

@section('css')
  <link rel="stylesheet" href="{{ URL::asset('css/views/auth/login.css') }}" />
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/auth/login.js') }}}"></script>
@endsection

@section('pagina')

  <div class="container-fluid">
    <div class="row no-gutter">
            
      <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image">
        <img class="rounded mx-auto my-auto d-block" src="{{URL::asset(env('APP_LOGO_EMSERH'))}}" height="80"/>
      </div>

      <div class="col-md-8 col-lg-6">
        <div class="login d-flex align-items-center py-5">
          <div class="container">
            <div class="row">
              <div class="col-md-9 col-lg-8 mx-auto">
                  
                <div class="text-center pb-3">
                  <img src="{{URL::asset(env('APP_LOGO_PRINCIPAL'))}}" class="img-responsive text-center" height="100"/>
                  <br/><br/>
                  <!-- <h3>{{env('APP_NAME')}}</h3> -->
                  <h3>Sistema Gerenciador de B.D</h3>
                </div>

                <!-- <h3 class="login-heading mb-4">Bem vindo ao SISACT!</h3> -->
                <form id="frmLogin" method="POST" action="{{ route('login') }}">
                  @csrf

                  <div class="form-label-group">
                    <input type="text" id="cpf" name="USUA_cpf" class="form-control mask_cpf" placeholder="CPF" required autofocus />
                    <label for="cpf">CPF</label>
                  </div>
  
                  <div class="form-label-group">
                    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Senha" required />
                    <label for="inputPassword">Senha</label>
                  </div>
  
                  <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember" id="remember" />
                    <label class="custom-control-label" for="customCheck1">Lembrar de mim</label>
                  </div>
                  <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Entrar</button>
                  <div class="text-center">
                    <a class="small" href="#">Esqueci minha senha</a></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
